
%%%%%%%%%%%%%%%%DESCRIPCI�N%%%%%%%%%%%%%%%%%%%%%%%%%

Se trata de una estructura autocontenida para poder utilizar
inmediatamente los scripts de matlab. Est� dise�ado para ser
ejecutado en un sistema UNIX. Los directorios deben mostrarse
con un '/' al final (p.ej.: pathSubj = '../Cross/')

Se parte de los datos descargados desde ADNI en formato .nii
y .xml habiendo juntado las dos carpetas que resultan de
descomprimir el fichero descargable de datos y metadatos.


Con los puntos 1-4 de las instrucciones se obtienen los datos
de las im�genes del paciente.




%%%%%%%%%%%%%%%%INSTRUCCIONES%%%%%%%%%%%%%%%%%%%%%
1.Ejecutar el script getNIIFiles()

2. Ejecutar el script recon_all()

3. Ejecutar el script recon_all_base()

4. Ejecutar el script recon_all_long()










