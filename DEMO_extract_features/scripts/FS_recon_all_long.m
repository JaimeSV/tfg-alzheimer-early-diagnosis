function FS_recon_all_long()

% Input:Cross and Base
%
% Output: Longitudinal segmentation

%% Preparation
%
% This is the path where the results of recon_all are. It must be the same
% directory as the SUBJ_DIR in Freesurfer
pathSubj = '../Cross/';

%The results of the recon-all -base command will be moved to this directory
BaseDir = '../Base/';

%The results of the recon-all -long command will be moved to this directory
LongDir = '../Long/';

listImg=dir(pathSubj);
numID=numel(listImg);


%% Image register
% For each subject the recon-all -long command is executed.
% The parpool tool is used to allow some computers to work in parallel.
% There are some issues between different Matlab versions.
% parfor is a for loop that enables parallelism

%parpool local 7;
parfor i=1:(numID)
%for i=1:numID
    if(length(listImg(i).name())>15)
        template=strcat(pathSubj,listImg(i).name(1:10));
        img=strcat(pathSubj,listImg(i).name());
        command = sprintf( 'recon-all -long %s %s -all\n',img,template);
        disp(command)
        system(command);

    end
end
%parpool close;


%% Avoiding undesirable directories
listImg=dir(pathSubj);
for i=1:numel(listImg)
    if(strcmp(listImg(i).name,'fsaverage')||strcmp(listImg(i).name,'lh.EC_average')||...
            strcmp(listImg(i).name,'rh.EC_average'))
        system(['unlink ' pathSubj,'',listImg(i).name]);
    end
end

%% Distributing the results data

listImg=dir(pathSubj);
for i=3:numel(listImg)
    orig=strcat(pathSubj, listImg(i).name);
    
    if(length(listImg(i).name)==10)
        dest=strcat(BaseDir,listImg(i).name());
        system(['mv ' orig, ' ' dest]);
        
    elseif(strcmp(listImg(i).name(end-14:end-11),'long'))
        dest=strcat(LongDir,listImg(i).name());
        system(['mv ' orig, ' ' dest]);
    end
end

end
