function FS_recon_all_base()
%% Description
% Input: norm.mgz of all images of the subject.
%
% Output: template of the subject

%% Preparation
%
% This is the path where the results of recon_all are. It must be the same
% directory as the SUBJ_DIR in Freesurfer
pathSubj = '../Cross/';

listFichImg = dir(pathSubj);
numID=numel(listFichImg);

row=1;
col=1;
n_imag=6;
listImg=cell(1,n_imag); % MAX number of visits!!

%% Avoiding non desirable directories

n=1;
for i=1:numID
    if(strcmp(listFichImg(i).name,'.')||strcmp(listFichImg(i).name,'..')||...
            strcmp(listFichImg(i).name,'fsaverage')||strcmp(listFichImg(i).name,'lh.EC_average')||...
            strcmp(listFichImg(i).name,'rh.EC_average'))
        index(n)=i;
        n=n+1;
    end
end

%% 
listFichImg(index)=[];
numID=numel(listFichImg);
template=listFichImg(1).name(1:10);

for i=1:numID
    if(strncmp(listFichImg(i).name(),template,10))
        listImg{row,col}=listFichImg(i).name();
        col=col+1;
    elseif(strncmp(listFichImg(i).name(),template,10)==0)
        template=listFichImg(i).name(1:10);
        row=row+1;
        listImg{row,1}=listFichImg(i).name();
        col=2;
    end
end

%% Template creation

numP=numel(listImg(:,1));

% The parpool tool is used to allow some computers to work in parallel.
% There are some issues between different Matlab versions.
% parfor is a for loop that enables parallelism

%parpool local 2;
parfor i=1:numP
% for i=1:numP
    aux=listImg{i,1}(1:10);
    for j=1:n_imag
        if(isempty(listImg{i,j})==0)
            aux=[aux, ' -tp ' pathSubj,listImg{i,j}]

        end
    end
    linecommand = sprintf( 'recon-all -base %s -all \n',aux);
    disp(linecommand)
    system(linecommand);
    
end
%parpool close;
end
