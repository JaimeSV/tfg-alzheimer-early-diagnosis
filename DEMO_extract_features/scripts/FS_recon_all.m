function FS_recon_all()
%% Description

%% Freesurfer previous inizialization
% export FREESURFER_HOME=/usr/local/freesurfer
% source /usr/local/freesurfer/SetUpFreeSurfer.sh
% export SUBJECTS_DIR='/home/jsimarro/Desktop/DEMO/Cross/'
% cd /home/jsimarro/Desktop/DEMO/scripts

%%Input: T1-MRI images in .nii.gz format
% This is the path where all the .nii.gz input files are stored.
pathId = '../Sorted/nii/'; 

%%Output:
% This is the path where all the data obtained from recon_all will be saved
% It must be the same directory as the SUBJ_DIR in Freesurfer
pathSubj = '../Cross/';
% This is the path were conflictive files will be saved
pathError = '../Error/'; 

%.nii.gz files are stored for further use in the code:
listFichImg = dir(strcat(pathId,'*.nii.gz'));
% Number of files is used for the main itteration.
numID=numel(listFichImg);

% Parfor is a for loop that enables parallelism
parfor i=1:numID 
%for i=1:numID
    %% Defining subject dir and aseg.stats path
    % This is be the directory inside SUBJ_DIR where all the data will be
    % saved.
    subjDir = strcat(pathSubj, listFichImg(i).name(1:end-7))   
    % The aseg.stats file is used to make sure that the process has been
    % succesful. It also avoids unnecessary itterations from past files.
    stats = strcat(subjDir,'/stats/aseg.stats');  
    if(exist(stats,'file')==0)
        %% Make subject dir
        makeDir =  sprintf ('mksubjdirs %s \n',subjDir);
        disp(makeDir)
        system(makeDir);             
        %% convert nii.gz -> mgz
        niiFile = strcat(pathId,listFichImg(i).name());
        mgzFile = strcat(subjDir,'/mri/orig/001.mgz');
        convert = sprintf ('mri_convert -it nii %s -ot mgz %s \n',niiFile, mgzFile);
        disp(convert)
        system(convert);
        %% Segmentation
        subjName = listFichImg(i).name(1:end-7);
        reconAll = sprintf( 'recon-all -s %s -all \n',subjName);
        disp(reconAll)
        system(reconAll);
        %% Check if exist stats to determine success
        if(exist(stats,'file')==0)
            disp('aseg.stats file could not be generated. Recon_all did not work properly');
            orig=strcat(pathSubj,subjName);
            dest=strcat(pathError,subjName);
            movefile(orig,dest);
        end
    else
        fprintf('%s: Recon_all was already executed on this file previously.\n',subjDir);
    end   
end
end
