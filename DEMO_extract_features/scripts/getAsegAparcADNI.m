function getAsegAparcADNI
% Generates files with clinical and demographic data * .dat, as well as information
% of subcortical structures (aseg) as cortical (aparc) in * .dat.
% It is prepared for the cross. For the long comment the cross code and
% uncomment the long.

%% Freesurfer variables
% export FREESURFER_HOME=/usr/local/freesurfer
% source /usr/local/freesurfer/SetUpFreeSurfer.sh
% export SUBJECTS_DIR=/home/jsimarro/Desktop/DEMO/Long
%% data
xmlPath= '../Sorted/xml/';
path_subj='../Long/';

% xmlPath='D:/compartido/Img/hipocampo/ADNI/XML_1386/';
% path_subj=[];
addpath(genpath('getAsegAparc'));


%% Cross

% typePross='Cross';
% name_dat='clinicaldata_cross.dat';
% QdecTable(xmlPath,path_subj,typePross,namae_dat);
% 
% command= strcat('asegstats2table --qdec',32,name_dat,32,'-t ./aseg_cross.',name_dat);
% disp(command);
% system(command);
% 
% command= strcat('aparcstats2table --qdec',32,name_dat,32,'-t ./aparc_cross_lh.',name_dat,32,'--hemi lh --meas thickness');
% disp(command);
% system(command);
% 
% command= strcat('aparcstats2table --qdec',32,name_dat,32,'-t ./aparc_cross_rh.',name_dat,32,'--hemi rh --meas thickness');
% disp(command);
% system(command);

%% Long

%% FreeSurfer variable (change SUBJECTS_DIR)
% export SUBJECTS_DIR=/home2/llin/Demo/Long/Long

typePross='Long';
name_dat='clinical_jsimarro_12M.dat';
QdecTable(xmlPath,path_subj,typePross,name_dat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Advice
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% To convert XLSX file import name_dat in Matlab and then export as table
% Next save the table by means 'writetable(name_dat_table,'table.xlsx');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


 
command= strcat('asegstats2table --qdec-long',32,name_dat,32,'-t ./aseg_long.',name_dat);
disp(command);
system(command);

command= strcat('aparcstats2table --qdec-long',32,name_dat,32,'-t ./aparc_long_lh.',name_dat,32,'--hemi lh --meas thickness');
disp(command);
system(command);

command= strcat('aparcstats2table --qdec-long',32,name_dat,32,'-t ./aparc_long_rh.',name_dat,32,'--hemi rh --meas thickness');
disp(command);
system(command);


rmpath(genpath('getAsegAparc'));


end

function QdecTable(xmlPath,path_subj,typePross,name_dat)
%% Generate QdecTable for LME Mass-univariate
% Input: Subject dir and xml dir
% Output: Qdectable Cross and Long

if(isempty(path_subj))
    strPatients=getPatientsInfor_b(xmlPath);
else
    strPatients=getPatientsInfor(path_subj,xmlPath);
end

% save strPatients strPatients
% load('strPatients.mat');

fsidbase=cell(numel(strPatients),1);
for i=1:numel(strPatients)
    fsidbase(i)=cellstr(strPatients(i).fsid(1:10));
end
strPatients=struct2table(strPatients);
varName=strPatients.Properties.VariableNames;
strPatients=table2cell(strPatients);
[sX,sY,ni,time_From_BL] = sortData(strPatients,3,strPatients,fsidbase);
sX(:,2)=time_From_BL;
strPatients=cell2table(sX,'VariableNames',varName);

if(strcmp(typePross,'Cross'))
writetable(strPatients,name_dat,'WriteRowNames',true,'delimiter', ' ');
else
% -----------------------------------------------Long-----------------------------------------------
fsidbase=cell2table(fsidbase);
strPatients=[strPatients(:,1),fsidbase,strPatients(:,2:end)];
%strPatients.Properties.VariableNames{'fsidbase'} = strcat('fsid',45,'base');
writetable(strPatients,name_dat,'WriteRowNames',true,'delimiter', ' ');
replace(name_dat,'fsidbase', 'fsid-base');

end
end

function strPatients=getPatientsInfor(path_subj,xmlPath)

listImgs= dir(strcat(path_subj));
listImgs=listImgs(3:end);   %   Remove . and ..
listFichXML = dir(strcat(xmlPath,'*.xml'));
numImgs = numel(listImgs);
strPatients(1,numImgs)=struct('fsid',[],'years',[],'Age',[],'sex',[],'MMSE',[],...
    'GDS',[],'CDR',[],'diagnose',[],'APOE_A1',[],'APOE_A2',[]);

for i=1:numImgs
    for j=1:numel(listFichXML)
        if(strcmp(listImgs(i).name(1:20),listFichXML(j).name(6:6+19)))
            %         %% XML
            [strPatients(i).sex,strPatients(i).Age,strPatients(i).MMSE,...
                strPatients(i).GDS,strPatients(i).CDR,strPatients(i).diagnose,...
                strPatients(i).fsid,strPatients(i).APOE_A1,strPatients(i).APOE_A2]=...
                read_XML(strcat(xmlPath,listFichXML(j).name));
            strPatients(i).fsid=listFichXML(j).name(6:end-4);
            fprintf('%s %s %.3f %s %.1f %d %d \n',listFichXML(j).name(6:end-4),strPatients(i).sex,strPatients(i).Age,...
                strPatients(i).diagnose,strPatients(i).MMSE,strPatients(i).APOE_A1,strPatients(i).APOE_A2);
        end
    end
    
end

end

function strPatients=getPatientsInfor_b(xmlPath)
listFichXML = dir(strcat(xmlPath,'*.xml'));
numImgs = numel(listFichXML);

strPatients(1,numImgs)=struct('fsid',[],'years',[],'Age',[],'sex',[],'MMSE',[],...
    'GDS',[],'CDR',[],'diagnose',[],'APOE_A1',[],'APOE_A2',[]);

for i=1:numImgs
    [strPatients(i).sex,strPatients(i).Age,strPatients(i).MMSE,...
        strPatients(i).GDS,strPatients(i).CDR,strPatients(i).diagnose,...
        strPatients(i).fsid,strPatients(i).APOE_A1,strPatients(i).APOE_A2]=...
        read_XML(strcat(xmlPath,listFichXML(i).name));
    strPatients(i).fsid=listFichXML(i).name(6:end-4);
    fprintf('%s %s %.3f %s %.1f %d %d \n',listFichXML(i).name(6:end-4),strPatients(i).sex,strPatients(i).Age,...
                strPatients(i).diagnose,strPatients(i).MMSE,strPatients(i).APOE_A1,strPatients(i).APOE_A2);
    
end

end


function replace(name_dat,S1,S2)

fid = fopen(name_dat,'rt') ;
X = fread(fid) ;
fclose(fid) ;

X = char(X.') ;
% replace string S1 with string S2
Y = strrep(X, S1, S2) ;
fid2 = fopen(name_dat,'wt') ;
fwrite(fid2,Y) ;
fclose (fid2) ;

end

% function [sex,Age,MMSE,GDS,CDR,diagnose,ID,APOE_A1,APOE_A2]=read_XML(filename)
% theStruct = parseXML(filename);
% sex =theStruct.Children(2).Children(8).Children(6).Children.Data;
% ID = str2double(theStruct.Children(2).Children(8).Children(16).Children(12).Children(8).Children(4).Children(2).Children.Data);
% try
%     MMSE=str2double(theStruct.Children(2).Children(8).Children(14).Children(4).Children(2).Children(2).Children.Data);
% catch
%     MMSE=-1;
% end
% 
% try
%     GDS=str2double(theStruct.Children(2).Children(8).Children(14).Children(6).Children(2).Children(2).Children.Data);
% catch
%     GDS=-1;
% end
% 
% try
%     CDR=str2double(theStruct.Children(2).Children(8).Children(14).Children(8).Children(2).Children(2).Children.Data);
% catch
%     CDR=-1;
% end
% 
% try
%     APOE_A1=str2double(theStruct.Children(2).Children(8).Children(10).Children.Data);
% catch
%     APOE_A1=-1;
% end
% try
%     APOE_A2=str2double(theStruct.Children(2).Children(8).Children(12).Children.Data);
% catch
%     APOE_A2=-1;
% end
% diagnose= theStruct.Children(2).Children(8).Children(8).Children.Data;
% Age=str2double(theStruct.Children(2).Children(8).Children(16).Children(4).Children.Data);
% 
% end
