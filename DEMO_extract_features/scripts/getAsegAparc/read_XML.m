function [sex,Age,MMSE,GDS,CDR,diagnose,ID,APOE_A1,APOE_A2]=read_XML(filename)
% This function is charged of finding the different parameters in the xml
% file, when possible. Default value for all of them is -1, and only if a
% valid value is found it will be changed.

%% Reading the xml file

theStruct = parseXML(filename);

% These parts of the struct are repeatedly used over the function.
data1 = theStruct.Children(2).Children(8).Children(14);
data2 = theStruct.Children(2).Children(8);

%% Guaranteed parameters
% These parameters are always expected to be found, and so they're not
% checked.

%% sex, id, diagnose, age
sex = data2.Children(6).Children.Data;
ID = str2double(data2.Children(16).Children(12).Children(8).Children(4).Children(2).Children.Data);
diagnose= data2.Children(8).Children.Data;
Age=str2double(data2.Children(16).Children(4).Children.Data);

%% Non guaranteed parameters
% These parameters are not always found. In this case, with a for loop the
% element matching the name of the parameter is found. I that is the case,
% the value will be changed from its default (-1).

%% MMSE, GDS, CDR

MMSE=-1;
GDS=-1;
CDR=-1;

for i=1:numel(data1.Children)
    % make sure the field is not empty
    Attr = data1.Children(i).Attributes;
    if isempty(Attr)
        continue;
    end
    % if the field is not empty, check for the value (name)
    if strcmp(Attr.Value , 'MMSE')
        MMSE=str2double(data1.Children(i).Children(2).Children(2).Children.Data);
    elseif strcmp(Attr.Value , 'GDSCALE')
        GDS=str2double(data1.Children(i).Children(2).Children(2).Children.Data);
    elseif strcmp(Attr.Value , 'CDR')
        CDR=str2double(data1.Children(i).Children(2).Children(2).Children.Data);
    end    
end

%% APOE_A1, APOE_A2

APOE_A1=-1;
APOE_A2=-1;

for i=1:numel(data2.Children)
    % make sure the field is not empty
    Attr = data2.Children(i).Attributes;
    if isempty(Attr)
        continue;
    end
    
    % if the field is not empty, check for the value (name)
    if strcmp(Attr.Value , 'APOE A1')
        APOE_A1 = str2double(data2.Children(i).Children.Data);
    elseif strcmp(Attr.Value , 'APOE A2')
        APOE_A2 = str2double(data2.Children(i).Children.Data);
    end    
end
end