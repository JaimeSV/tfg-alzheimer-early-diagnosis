addpath('export_fig')
clear;
clc;
close all;
if 0
    pref='Figuras/Exp_A.1.1';
    load('668_cluster_with_MCI/Results_k_fold_with_MCI_3000.mat');
elseif 1
    
    pref='Figuras/Exp_A.1.2';
    load('all_MCI/All_k_fold_with_MCI_1000.mat');
elseif 0
    pref='Figuras/Exp_A.2';
    load('668_cluster_with_MCI_AD/Results_k_fold_with_MCI_1000.mat');
elseif 0
    pref='Figuras/Exp_B';
    load('668_select_with_AD/Results_k_fold_with_AD.mat');
    Complete_results=Complete_results_total;
    key_feat=key_feat_total;
elseif 0
    pref='Figuras/Exp3_all_MCI_';
    load('Results_k_fold_with_AD.mat');    
end
visit=1;
sort_and_print(best_combinations,best_stat(:,:,visit),features_names);

% clear 'Complete_results', 'key_feat';
% key_feat=[];
% scores=[];
% best_stat(:,:,1)=[];
% %% Sorted by dimension
% n_zeros=sum(best_combinations==0,2);
% un_index=unique(n_zeros);
% for n_visit=1:size(best_stat,3)
% for k=1:size(un_index,1)
%     index_same=find(un_index(k)==n_zeros);
%     key_feat{k,n_visit}=best_combinations(index_same,:);
%     scores{k,n_visit}=best_stat(index_same,:,n_visit);    
% end
% end
% %% Switching the order
% save_key_feat=[];
% for d=1:size(scores,2)
%     new_Complete_results(d,:)=scores(end-d+1,:);
%     new_key_feat(d,1)=key_feat(1,end-d+1);
% %     save_key_feat=[save_key_feat;key_feat{1,end-d+1}];   
% end
% clear 'Complete_results', 'key_feat';
% addpath(genpath('../'));
% 
% lenght=3;
% choose=5; %¿Acc?
% if 1
% name_visitas=strcat(pref,'k_fold_dimensiones.pdf');
% f1=figure('Position',[2 200 1000 2500]);
% for dim=1:size(new_Complete_results,2)
%     for n_visit=1:size(best_stat,3)
%         aux=new_Complete_results{n_visit,dim};
%         mean_visit_result(n_visit)=mean(aux(:,choose));
%         std_visit_result(n_visit)=std(aux(:,choose));
%         % visit_key(n_visit)=one_key(1:lenght,:);
%     end
%         subplot(2,2,dim)
%         barwitherr(std_visit_result',mean_visit_result')
%         box on;
%         hold on
%         plot(mean_visit_result,'LineWidth',5)
%         if dim==1
%             title(sprintf('% i-Dimensiones',dim+1))
%         elseif dim==11
%             title(sprintf('% i-Dimensiones',15))
%         else
%             title(sprintf('% i-Dimensiones',dim+1))
%             
%             
%         end
%         %         xlabel('n de visitas');
%         %         ylabel('Accuracy')
%         %         xlabel("Number of visits")
%         % ylabel("Accuray")
%         grid on
%         set(gcf,'color','white')
%     end
% end
% export_fig(name_visitas);
% if 1
% name_dimensiones=strcat(pref,'k_fold_visitas_Masivo.pdf');
% f1=figure('Position',[2 200 1000 2500]);
% for n_visit=1:size(new_Complete_results,1)
%     for dim=1:size(new_Complete_results,2)
%         
%         %% Sorted by frequency
%         [~,index]=sort(new_Complete_results{n_visit,dim},1,'descend');
%         one_results=new_Complete_results{n_visit,dim};
%         %    [a,b]= unique(index(:,7));
%         one_results=one_results(index(:,7),:);
%         one_key=new_key_feat{dim};
%         one_key=one_key(index(:,7),:);
%         %%
%         best_dim_result(:,dim)=one_results(1:lenght,choose);
%         %         aaaux_dim_result=one_results(:,choose).*one_results(:,7);
%         %         aamean_dim_result(:,dim)=sum(aaaux_visit_result)/sum(one_results(:,7));
%         %%
%         cnt=0;
%         for i=1:size(one_results,1)
%             for j=1:one_results(i,7)
%                 cnt=cnt+1;
%                 aux_dim_result(cnt)=one_results(i,choose);
%             end
%         end
%         mean_dim_result(dim)=mean(aux_dim_result);
%         std_dim_result(dim)=std(aux_dim_result);
%         %     visit_key(n_visit)=one_key(1:lenght,:);
%     end
%     subplot(2,2,n_visit)
%     barwitherr(std_dim_result',mean_dim_result')
%     box on;
%     grid on;
%     hold on
%     plot(mean_dim_result,'LineWidth',5)
%     
%     if n_visit==1
%         title('Baseline')
%     else
%         title(sprintf('% i Visitas',n_visit))
%     end
%     %         xlabel('n de visitas');
%     %         ylabel('Accuracy')
%     set(gcf,'color','white')
%     %xticklabels({'1','2','3','4','5','6','7','8','9','10','15'});
%     xticklabels({'2','3','4','5','6','7','8','9','10','15'});
%     
% end
% export_fig(name_dimensiones);
% end
% 
%  
function sort_and_print(gr_feat,stat_feat,features_names)
%% Looking for same features in order to mean scores and know frequency
[selection,~,index]=unique(gr_feat,'rows');
un_index=unique(index);
mean_scores=zeros(size(un_index,1),7);
features_names = regexprep(features_names, '_', '\\_');

for f=1:size(un_index,1)
    index_same=find(un_index(f)==index);
    if size(index_same,1)>50
    select=selection(f,:);
    select(selection(f,:)==0)=[];
    
    name=features_names{select(1)};
    for n=2:size(select,2)
        name=strcat(name,' +  ',features_names{select(n)});
    end
    printfResults(stat_feat(index_same,:),name)
    end
end
end


function printfResults(stat_LME,marker)
SEN_Su_LME = confidenceIntervals(stat_LME(:,1))*100;
SPE_Su_LME = confidenceIntervals(stat_LME(:,2))*100;
ACC_Su_LME = confidenceIntervals(stat_LME(:,5))*100;
AUC_Su_LME = confidenceIntervals(stat_LME(:,6));
fprintf(' %.1f (%.1f,%.1f) & %.1f (%.1f,%.1f) & %.1f (%.1f,%.1f) &  %.3f (%.3f,%.3f)\t & $ %s $ \\\\ \n',...
    mean(stat_LME(:,1))*100,SEN_Su_LME(1),SEN_Su_LME(2),mean(stat_LME(:,2))*100,SPE_Su_LME(1),SPE_Su_LME(2),...
    mean(stat_LME(:,5))*100,ACC_Su_LME(1),ACC_Su_LME(2),mean(stat_LME(:,6)),AUC_Su_LME(1),AUC_Su_LME(2),marker);
end
function CI = confidenceIntervals(x)
SEM = std(x)/sqrt(length(x));               % Standard Error
ts = tinv([0.025  0.975],length(x)-1);      % T-Score
CI = mean(x) + ts*SEM;                      % Confidence Intervals
end
