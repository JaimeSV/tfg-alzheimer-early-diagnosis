
addpath('export_fig')
clear;
clc
close all;
if 1
    pref='Figuras/Exp_A.1.1';
    load('668_cluster_with_MCI/Results_k_fold_with_MCI_3000.mat');
elseif 0
    pref='Figuras/Exp_A.1.2';
    load('all_MCI/All_k_fold_with_MCI_1000.mat');
elseif 0
    pref='Figuras/Exp_A.2';
    load('668_cluster_with_MCI_AD/Results_k_fold_with_MCI_1000.mat');
elseif 0
    pref='Figuras/Exp_B';
    load('668_select_with_AD/Results_k_fold_with_AD.mat');
    Complete_results=Complete_results_total;
    key_feat=key_feat_total;
elseif 0
    pref='Figuras/Exp3_all_MCI_';
    load('Results_k_fold_with_AD.mat');    
end

% visit=1;
% sort_and_print(best_combinations,best_stat(:,:,visit),features_names);


%% Switching the order
save_key_feat=[];
for d=1:size(Complete_results,2)
    new_Complete_results(:,d)=Complete_results(:,end-d+1);
    new_key_feat(1,d)=key_feat(1,end-d+1);
    save_key_feat=[save_key_feat;key_feat{1,end-d+1}];
    
end

% save('key_feat_k_fold','save_key_feat')


clear 'Complete_results', 'key_feat';
addpath(genpath('../'));

lenght=4;
choose=5; %¿Acc?

name_visitas=strcat(pref,'_dimen.pdf');
f1=figure('Position',[2 200 1000 2500]);
for dim=1:size(new_Complete_results,2)
    for n_visit=1:size(new_Complete_results,1)
        %% Sorted by frequency
        [~,index]=sort(new_Complete_results{n_visit,dim},1,'descend');
        one_results=new_Complete_results{n_visit,dim};
        %    [a,b]= unique(index(:,7));
        one_results=one_results(index(:,7),:);
        one_key=new_key_feat{dim};
        one_key=one_key(index(:,7),:);
        %%
        best_visit_result{n_visit,dim}=one_results(1:lenght,:);
        best_visit_key{n_visit,dim}=one_key(1:lenght,:);
        %%
        cnt=0;
        for i=1:size(one_results,1)
            for j=1:one_results(i,7)
                cnt=cnt+1;
                aux_visit_result(cnt)=one_results(i,choose);
            end
        end
        mean_visit_result(n_visit)=mean(aux_visit_result);
        std_visit_result(n_visit)=std(aux_visit_result);
        % visit_key(n_visit)=one_key(1:lenght,:);
    end
    if 1
        subplot(2,2,dim)
        barwitherr(std_visit_result',mean_visit_result')
        box on;
        hold on
        plot(mean_visit_result,'LineWidth',5)
        if dim==1
            title(sprintf('% i-Dimensiones',dim+1))
        elseif dim==11
            title(sprintf('% i-Dimensiones',15))
        else
            title(sprintf('% i-Dimensiones',dim+1))
        end
        ylim([0,1])
        %         xlabel('n de visitas');
        %         ylabel('Accuracy')
        %         xlabel("Number of visits")
        % ylabel("Accuray")
        grid on
        set(gcf,'color','white')
    end
end
export_fig(name_visitas);
if 1
    name_dimensiones=strcat(pref,'_visit.pdf');
    f1=figure('Position',[2 200 1000 2500]);
    for n_visit=1:size(new_Complete_results,1)
        for dim=1:size(new_Complete_results,2)
            
            %% Sorted by frequency
            [~,index]=sort(new_Complete_results{n_visit,dim},1,'descend');
            one_results=new_Complete_results{n_visit,dim};
            %    [a,b]= unique(index(:,7));
            one_results=one_results(index(:,7),:);
            one_key=new_key_feat{dim};
            one_key=one_key(index(:,7),:);
            %%
            best_dim_result(:,dim)=one_results(1:lenght,choose);
            %         aaaux_dim_result=one_results(:,choose).*one_results(:,7);
            %         aamean_dim_result(:,dim)=sum(aaaux_visit_result)/sum(one_results(:,7));
            %%
            cnt=0;
            for i=1:size(one_results,1)
                for j=1:one_results(i,7)
                    cnt=cnt+1;
                    aux_dim_result(cnt)=one_results(i,choose);
                end
            end
            mean_dim_result(dim)=mean(aux_dim_result);
            std_dim_result(dim)=std(aux_dim_result);
            %     visit_key(n_visit)=one_key(1:lenght,:);
        end
        subplot(2,2,n_visit)
        barwitherr(std_dim_result',mean_dim_result')
        box on;
        grid on;
        hold on
        plot(mean_dim_result,'LineWidth',5)
        
        if n_visit==1
            title('Baseline')
        else
            title(sprintf('% i Visitas',n_visit))
        end
        %         xlabel('n de visitas');
        %         ylabel('Accuracy')
        set(gcf,'color','white')
        %xticklabels({'1','2','3','4','5','6','7','8','9','10','15'});
        xticklabels({'2','3','4','5','6','7','8','9','10','15'});
        ylim([0,1]);
        
    end
    export_fig(name_dimensiones);
end
print_visit=1;
for l=1:lenght
    fprintf('\n %i POSICION \n',l);
    fprintf('\tSEN \t\t SPE \t\t  ACC \t\t AUC \t\tFREQ \n'); %%a[sensitivy,specificity,PPV,NPV,ACC,auc];
    
    for p=1:size(best_visit_result,2)
        print_results=best_visit_result{print_visit,p};
        print_key=best_visit_key{print_visit,p};
        printfResults(print_results(l,:),p,print_key(l,:),features_names);
    end
end




function printfResults(stat_LME,dim,marker,names)
SEN_Su_LME = confidenceIntervals(stat_LME(:,1))*100;
SPE_Su_LME = confidenceIntervals(stat_LME(:,2))*100;
ACC_Su_LME = confidenceIntervals(stat_LME(:,5))*100;
AUC_Su_LME = confidenceIntervals(stat_LME(:,6));
marker(marker==0)=[];
fprintf('%i Dimension: %.1f (%.1f,%.1f) %.1f (%.1f,%.1f) %.1f (%.1f,%.1f) %.3f (%.3f,%.3f) %i \t',dim+1,...
    mean(stat_LME(:,1))*100,SEN_Su_LME(1),SEN_Su_LME(2),mean(stat_LME(:,2))*100,SPE_Su_LME(1),SPE_Su_LME(2),...
    mean(stat_LME(:,5))*100,ACC_Su_LME(1),ACC_Su_LME(2),mean(stat_LME(:,6)),AUC_Su_LME(1),AUC_Su_LME(2),stat_LME(:,7));
for m=1:size(marker,2)
    fprintf('%s  ',names{marker(m)});
end
fprintf('\n')
end


function CI = confidenceIntervals(x)
SEM = std(x)/sqrt(length(x));               % Standard Error
ts = tinv([0.025  0.975],length(x)-1);      % T-Score
CI = mean(x) + ts*SEM;                      % Confidence Intervals
end



