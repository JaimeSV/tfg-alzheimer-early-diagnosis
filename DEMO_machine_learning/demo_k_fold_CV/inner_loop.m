function [best_combinations] =...
    inner_loop(features,group,X,ni,dim_feat,n_boot, k_first_selected_groups)
%mean_combinations and best_combinations show the mean accuracy value and
%the best result (according to a accuracy+balance criteria.
% best_dim: features dimension with best results
% general_best_combination: combination with best results
% best_combinations: vector of best combinations for each dimension
% best_values: best error values for each dimension
% mean_ACC: mean accuracy for each dimension


% dim_feat is a vector with the different dimensions studied.

% n_boot is the number of iterations to run the
% groupFeatureSelectionADNI_ROI function

% k_first_selected_groups is the number of different combinations in order to obtain a
% statistical result of the error for one dimension.


%For each iteration, featureSelectionADNI_ROI will be called. Once the
%best features are obtained, groupFeatureSelection is executed to obtain
%the error values. Then, the best dimension value is chosen.


size_dim=length(dim_feat);
labels=cell(size_dim,1);
%stat_feat=cell(size_dim,1);
best_combinations=zeros(size_dim,dim_feat(end));
best_values=zeros(size_dim,7);
mean_ACC=zeros(size_dim,1);
dimensionaty=[];
ACC=[];
key_feat=cell(size_dim,1);
scores=cell(size_dim,1);
stat_feat=[];
gr_feat=[];


for i=1:n_boot
        fprintf('Inner loop %i \n',i)
    [mask_scan_train,mask_scan_test,ni_two_train,ni_two_test]...
        =splitSamples(ni,.90,-1,group);
    group_train=group(mask_scan_train);
    select_features=[];
    for dim=1:size_dim
        select_features_dim=featureSelection(features(mask_scan_train,:),group_train,ni_two_train,dim_feat(dim),k_first_selected_groups);
        select_features_dim=[select_features_dim,zeros(size(select_features_dim,1),dim_feat(end)-size(select_features_dim,2))];
        select_features=[ select_features; select_features_dim];
    end

    [stat_i,~]=LME_long_error(features,select_features,X,...
        mask_scan_train,mask_scan_test,ni_two_train,ni_two_test,1); %% By default will choose the best features with data from baseline the visits
    stat_feat=[stat_feat;stat_i];
    gr_feat=[gr_feat;select_features];
end

[key_feat,scores]=sort_dimension_freq(gr_feat,stat_feat);
for i=1:size(key_feat,2)
% Choosing best results and their combinations of features.
[best_values(i,:),idx] = choosebest(scores{i});
best_combinations(i,:)=key_feat{i}(idx,:);
end
% %% SAVING DATA
% save('features_Selected.mat','key_feat','scores','best_values','best_combinations');

end


function [best_values,idx] = choosebest(mean_scores)
% This function expects a matrix where the rows are different combinations
% and columns are 6 statistics of error:
%sensibility,specifity,PPV,NPV,accuracy,area_under_curve,frequency

%default value has the best accuracy
[sorted_scores,sortedidx]=sortrows(mean_scores,5,'descend'); 
 best_values = sorted_scores(1,:);
 idx=sortedidx(1);

for i=1:size(sortedidx,1)
    if(sorted_scores(i,1)>0.7 && sorted_scores(i,2)>0.7 && sorted_scores(i,7)>2) %% The best combinations must have a high frequency
        best_values = sorted_scores(i,:);
        idx=sortedidx(i);
        break;        
    end    
end
end
