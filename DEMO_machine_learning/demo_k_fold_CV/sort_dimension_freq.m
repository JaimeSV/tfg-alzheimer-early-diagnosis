function[key_feat,scores]=sort_dimension_freq(gr_feat,stat_feat)
%% Looking for same features in order to mean scores and know frequency
[selection,~,index]=unique(gr_feat,'rows');
un_index=unique(index);
mean_scores=zeros(size(un_index,1),7);
for f=1:size(un_index,1)
    index_same=find(un_index(f)==index);
    mean_scores(index(index_same(1)),1:end-1)=mean(stat_feat(index_same,:),1);
    mean_scores(index(index_same(1)),end)=size(index_same,1);
end
%% Sorted by dimension
n_zeros=sum(selection==0,2);
un_index=unique(n_zeros);
for k=1:size(un_index,1)
    index_same=find(un_index(k)==n_zeros);
    key_feat{k}=selection(index_same,:);
    scores{k}=mean_scores(index_same,:);    
end

end