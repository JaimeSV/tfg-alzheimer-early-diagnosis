function [] = k_fold_CV()
% Complete pipeline of LME

addpath(genpath('../lme/'));
addpath(genpath('../Auxiliar'));

n_visit=4;
n_outer_loop=1000;

warning ('off','all');
dim_feat=[2:5];
n_inner_loop=5;
selection_with_AD=false;
cluster_MCI=true;
cluster_AD=false;
nitest=-1;  % nitest is a parameter to choose the number of images chosen for the test.
% if nitest=-1, default behaviour is to take all visits;

%% Data
[Tresults_MCI,features_MCI,features_names,ni_MCI] = getData_Jaime('my_MCI',cluster_MCI,cluster_AD);  %groups,cl_MCI,cl_AD
fprintf('Be careful!!!!!! Warnings are disable!\n')
best_combinations=[];
best_stat=[];
for loop=1:n_outer_loop
    %clc;
    fprintf('Outer loop %i \n',loop)
    [mask_scan_selection,mask_scan_test,ni_two_model,ni_two_test]...
        =splitSamples(ni_MCI,.90,nitest,Tresults_MCI);
    
    if selection_with_AD
        [selection_Tresults,selection_features,~,ni_two_selection,~] = getData_Jaime('NC_AD',cluster_MCI,cluster_AD); 
        name='Results_k_fold_with_AD.mat';
    else
        selection_features=features_MCI(mask_scan_selection,:);
        selection_Tresults=Tresults_MCI(mask_scan_selection,:);
        ni_two_selection=ni_two_model;
        name='Results_k_fold_with_MCI.mat';
    end
    
    
    %% First we check for the best features:
    [selection_X,selection_group,message_FixedEffects]=getFixedEffects(selection_Tresults);
    
    if ~selection_with_AD || ~exist('best_combinations_i','var')
        [best_combinations_i] =...
            inner_loop(selection_features,selection_group,selection_X,ni_two_selection,dim_feat,n_inner_loop,5);
    end
    
    %% Then we run iterations with the very best feature combinations:
    [test_X]=getFixedEffects(Tresults_MCI);
    [best_stat_i,~]=LME_long_error(features_MCI,best_combinations_i,test_X,...
        mask_scan_selection,mask_scan_test,ni_two_model,ni_two_test,n_visit);
    
    best_stat=[best_stat;best_stat_i];
    best_combinations=[best_combinations;best_combinations_i];
    
    if mod(loop,50)==0    
        for v=1:n_visit
            [key_feat(v,:),Complete_results(v,:)]=sort_dimension_freq(best_combinations,best_stat(:,:,v));
        end
        name_loop=strcat(name(1:end-4),'_',num2str(loop),'.mat');
        save(name_loop,'key_feat','Complete_results','n_visit','message_FixedEffects','features_names','best_stat','best_combinations');
    end
    
end
for v=1:n_visit
    [key_feat_total(v,:),Complete_results_total(v,:)]=sort_dimension_freq(best_combinations,best_stat(:,:,v));
end
%% SAVING DATA
save(name,'key_feat_total','Complete_results_total','n_visit','message_FixedEffects','features_names','best_stat','best_combinations');

rmpath('../lme/');
rmpath('../Auxiliar/');
warning('on','all');
end
