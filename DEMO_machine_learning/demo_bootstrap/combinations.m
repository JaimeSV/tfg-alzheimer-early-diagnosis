%% Given maximum number of group and a sure markers, generate all the different combinations
function [names_features_selected,gr_feat,features,index_zero]=combinations(features_names,all_features,n_max_group,sure_markers)
gr_feat=[];
names_features_selected={};
for n_group=1:n_max_group
C = nchoosek(1:size(features_names,2)-size(sure_markers,2),n_group);
if ~isempty(sure_markers)
    for i=1:size(sure_markers,2)
        index=find(strcmp(sure_markers{i},features_names));
        C(:,n_group+i)=index;
    end
end
if ~isempty(gr_feat)
    
    while size(gr_feat,2)<size(C,2)
        gr_feat(:,end+1)=0;
    end
end
for j=1:size(C,1)
    [gr_feat,names_features_selected]=select_features(gr_feat,names_features_selected,features_names,C(j,:));
end
end
%% Features will be a vector made only with selected features
% So its necessary change index in gr_feat 
[features_names,~,c]=unique(names_features_selected,'stable');
[index_features,~,b]=unique(gr_feat,'stable');
if c~=b
   error('Error in unique'); 
end
gr_feat=reshape(b,size(gr_feat,1),size(gr_feat,2));
features=zeros(size(all_features,1),size(index_features,1));
for i=1:size(index_features,1) % Its necessary because there is a zero index
    if index_features(i)~=0
            features(:,i)=all_features(:,index_features(i));
    else
        index_zero=i;
    end
end
names_features_selected=features_names(gr_feat);
end

function[gr_feat,names_features_selected]=select_features(gr_feat,names_features_selected,features_names,features_selected)
gr_feat=[gr_feat;features_selected];
features_selected(:,~any(features_selected,1))=[]; % Removes all the columns with zero
if ~isempty(names_features_selected)
    while size(names_features_selected,2)<size(features_selected,2)
        names_features_selected(:,size(names_features_selected,2)+1)={'.'};
    end
end
names_features_selected=[names_features_selected;{features_names{features_selected}}];
end
