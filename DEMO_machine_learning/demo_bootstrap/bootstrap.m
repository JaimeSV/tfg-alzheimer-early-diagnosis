function [stat_feat]=bootstrap
addpath(genpath('../lme'));
addpath(genpath('../Auxiliar'));
 name='Results_bootstrap_with_clusters_from_both.mat';

%% FEATURE SELECTION
%[names_features_selected,gr_feat,features,index_zero]=combinations(features_names,all_features,10,{'NHV'});
% [Tresults_AD,features_AD,features_names_AD,ni_AD]=getData_Jaime('NC_AD_with_cluster_from_both');
% group_AD=strcmp(Tresults_AD.diagnose,'AD');
% dim_max=10;
% row_max=25;
% select_features=[];
% for dim=[1:10,15:5:dim_max]
%     %select_features_dim=featureSelectionADNI_ROI_JAIME(features(mask_scan_train,:),group_train,ni_two_train,dim_feat(dim),k_first_selected_groups);
%     select_features_dim=featureSelection(features_AD,group_AD,ni_AD,dim,row_max);
%     select_features_dim=[select_features_dim,zeros(size(select_features_dim,1),dim_max(end)-size(select_features_dim,2))];
%     select_features=[ select_features; select_features_dim];
% end
% fprintf('Numero de grupos:%i\n',size(select_features,1));
load key_feat_k_fold;
select_features=save_key_feat;
%% Fixed Effects
[Tresults,features,features_names,ni] = getData_Jaime('MCI_with_cluster_from_both');

% if ~isequal(features_names,features_names_AD)
%     error('Error selection and training dont match')
% end

[X,group,message_FixedEffects]=getFixedEffects(Tresults);

num_groups=size(select_features,1);
numSamples=size(features,1);
%% Clasification

n_boot=1000;

stat_feat=zeros(num_groups,6,n_boot);
parfor i=1:n_boot
    clc;
    fprintf('Sample %d\n',i);
    [mask_scan_train,mask_scan_test,ni_two_train,ni_two_test]=splitSamples(ni,.75,-1,group);
    n_visit=1; % We want data from baseline
    [stat_i,~]=LME_long_error(features,select_features,X,...
        mask_scan_train,mask_scan_test,ni_two_train,ni_two_test,n_visit); 
    stat_feat(:,:,i)=stat_i;
   if mod(i,100)==0
        name_loop=strcat(name(1:end-4),'_',num2str(i),'.mat');
        save(name_loop,'stat_feat','select_features','message_FixedEffects','features_names');
    end
end
save(name,'stat_feat','select_features','message_FixedEffects','features_names');
rmpath('../lme');
rmpath('../Auxiliar');
end


