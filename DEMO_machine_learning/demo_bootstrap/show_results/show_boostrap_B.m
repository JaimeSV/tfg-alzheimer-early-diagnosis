addpath('export_fig')
clear;
clc;
close all;
 load Results_bootstrap_with_clusters_from_both_1000.mat

visit=1;
Complete_results=stat_feat;
key_feat=select_features;
%% Looking for same features in order to mean scores and know frequency
% [selection,~,index]=unique(gr_feat,'rows');
% un_index=unique(index);
% mean_scores=zeros(size(un_index,1),7);
% features_names = regexprep(features_names, '_', '\\_');
for d=1:size(Complete_results,2)
    new_Complete_results(:,d)=Complete_results(:,end-d+1);
    new_key_feat(1,d)=key_feat(1,end-d+1);
%     save_key_feat=[save_key_feat;key_feat{1,end-d+1}];
    
end
  %% Sorted by frequency
        [~,index]=sort(new_Complete_results,1,'descend');
        one_results=new_Complete_results;
        %    [a,b]= unique(index(:,7));
        one_results=one_results(index(:,7),:);
        one_key=new_key_feat;
        one_key=one_key(index(:,7),:);
        %%
        best_visit_result{n_visit,dim}=one_results(1:lenght,:);
        best_visit_key{n_visit,dim}=one_key(1:lenght,:);
        %%
        cnt=0;
        for i=1:size(one_results,1)
            for j=1:one_results(i,7)
                cnt=cnt+1;
                aux_visit_result(cnt)=one_results(i,choose);
            end
        end
        mean_visit_result(n_visit)=mean(aux_visit_result);
        std_visit_result(n_visit)=std(aux_visit_result);
