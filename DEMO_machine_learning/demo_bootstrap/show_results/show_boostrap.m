close all;
clear;
clc;
 %load Results_bootstrap_with_clusters_from_both_1000.mat
%load Results_bootstrap_from_key_feat.mat

load New_bootstrap_with_key.mat
 stat_feat=stat_feat([43,92,262,628],:,:);
 select_features=select_features([43,92,262,628],:);
% load Results_bootstrap_900.mat
%load Results_bootstrap_with_AD_1000.mat
%  stat_feat=stat_feat([1,7,9,13,22,23,28,34,39,45],:,:);
%  select_features=select_features([1,7,9,13,22,23,28,34,39,45],:);
mean_scores=mean(stat_feat,3); % sensitivy,specificity,PPV,NPV,ACC,auc
num_groups=size(mean_scores,1);

% figure()
% hold on;
% std_scores=std(stat_feat,0,3);
% errorbar(1:num_groups,mean_scores(:,1),std_scores(:,1),'r');
% errorbar(1:num_groups,mean_scores(:,2),std_scores(:,2),'g');
% errorbar(1:num_groups,mean_scores(:,5),std_scores(:,5),'r');
% errorbar(1:num_groups,mean_scores(:,6),std_scores(:,6),'k');
% legend('SEN','SPE','ACC','AUC');
% hold off;
 f1=figure('Position',[2 200 1500 500]);
plot(1:num_groups,mean_scores(:,1),'r',1:num_groups,mean_scores(:,2),'g',...
    1:num_groups,mean_scores(:,5),'b',1:num_groups,mean_scores(:,6),'k');
legend('SEN','SPE','ACC','AUC');

fprintf('\tSEN \t\t SPE \t\t\t ACC \t\t AUC\n');
features_names = regexprep(features_names, '_', '\\_');

for f=1:size(select_features,1)
    %index_same=find(un_index(f)==index);
    select=select_features(f,:);
    select(select_features(f,:)==0)=[];    
    name=features_names{select(1)};
    for n=2:size(select,2)
        name=strcat(name,' +  ',features_names{select(n)});
    end
    printfResults(squeeze(stat_feat(f,:,:))',name,f)
    
end


function printfResults(stat_LME,marker,f)
SEN_Su_LME = confidenceIntervals(stat_LME(:,1))*100;
SPE_Su_LME = confidenceIntervals(stat_LME(:,2))*100;
ACC_Su_LME = confidenceIntervals(stat_LME(:,5))*100;
AUC_Su_LME = confidenceIntervals(stat_LME(:,6));
fprintf('%i %.1f (%.1f,%.1f) & %.1f (%.1f,%.1f) & %.1f (%.1f,%.1f) &  %.3f (%.3f,%.3f)\t & $ %s $ \\\\ \n',...
    f,mean(stat_LME(:,1))*100,SEN_Su_LME(1),SEN_Su_LME(2),mean(stat_LME(:,2))*100,SPE_Su_LME(1),SPE_Su_LME(2),...
    mean(stat_LME(:,5))*100,ACC_Su_LME(1),ACC_Su_LME(2),mean(stat_LME(:,6)),AUC_Su_LME(1),AUC_Su_LME(2),marker);
end
function CI = confidenceIntervals(x)
SEM = std(x)/sqrt(length(x));               % Standard Error
ts = tinv([0.025  0.975],length(x)-1);      % T-Score
CI = mean(x) + ts*SEM;                      % Confidence Intervals
end

function [] =save_fig(name)
box off;
name_path=strcat('/home/jaime/Escritorio/TFG_V4/Documento_TFG/figuras/resultados/',name,'.pdf');
set(gcf,'color','white')
export_fig(name_path);    
end