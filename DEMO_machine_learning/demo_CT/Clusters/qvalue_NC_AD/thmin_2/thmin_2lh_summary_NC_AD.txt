# Cluster Growing Summary (mri_surfcluster)
# $Id: mri_surfcluster.c,v 1.51.2.3 2012/05/31 22:10:05 greve Exp $
# $Id: mrisurf.c,v 1.693.2.7 2013/05/12 22:28:01 nicks Exp $
# CreationTime 2018/06/15-11:03:07-GMT
# cmdline mri_surfcluster --in Models/q_map_NC_AD_LH.mgh --subject /fsaverage --hemi lh --thmin 2 --sign pos --no-adjust --minarea 50 --cwsig Clusters/qvalue_NC_AD/thmin_2lh_clusters_NC_AD.mgh --sum Clusters/qvalue_NC_AD/thmin_2lh_summary_NC_AD.txt --olab Clusters/qvalue_NC_AD/thmin_2lh_label_NC_AD 
# cwd /home/jsimarro/Desktop/demoCT
# sysname  Linux
# hostname copernico
# machine  x86_64
# FixVertexAreaFlag 1
# FixSurfClusterArea 1
# 
# Input      Models/q_map_NC_AD_LH.mgh
# Frame Number      0
# srcsubj /fsaverage
# hemi lh
# surface white
# SUBJECTS_DIR /usr/local/freesurfer/subjects
# SearchSpace_mm2 65416.6
# SearchSpace_vtx 163842
# Bonferroni 0
# Minimum Threshold 2
# Maximum Threshold infinity
# Threshold Sign    pos
# AdjustThreshWhenOneTail 0
# Area Threshold    50 mm^2
# Overall max inf at vertex 603
# Overall min -1.07857 at vertex 50788
# NClusters          3
# Total Cortical Surface Area 65416.6 (mm^2)
# FixMNI = 1
# 
# ClusterNo  Max   VtxMax   Size(mm^2)  TalX   TalY   TalZ    NVtxs
   1          inf     603  57361.20    -22.0   -8.5  -26.0  111942
   2        4.234  130755    401.61    -12.9  -83.8    9.7   716
   3        2.348   77926     52.18    -45.4  -23.6   21.6   131
