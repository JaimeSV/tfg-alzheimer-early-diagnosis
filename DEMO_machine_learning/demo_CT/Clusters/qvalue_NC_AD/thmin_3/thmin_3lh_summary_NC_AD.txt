# Cluster Growing Summary (mri_surfcluster)
# $Id: mri_surfcluster.c,v 1.51.2.3 2012/05/31 22:10:05 greve Exp $
# $Id: mrisurf.c,v 1.693.2.7 2013/05/12 22:28:01 nicks Exp $
# CreationTime 2018/06/15-11:11:43-GMT
# cmdline mri_surfcluster --in Models/q_map_NC_AD_LH.mgh --subject /fsaverage --hemi lh --thmin 3 --sign pos --no-adjust --minarea 50 --cwsig Clusters/qvalue_NC_AD/thmin_3lh_clusters_NC_AD.mgh --sum Clusters/qvalue_NC_AD/thmin_3lh_summary_NC_AD.txt --olab Clusters/qvalue_NC_AD/thmin_3lh_label_NC_AD 
# cwd /home/jsimarro/Desktop/demoCT
# sysname  Linux
# hostname copernico
# machine  x86_64
# FixVertexAreaFlag 1
# FixSurfClusterArea 1
# 
# Input      Models/q_map_NC_AD_LH.mgh
# Frame Number      0
# srcsubj /fsaverage
# hemi lh
# surface white
# SUBJECTS_DIR /usr/local/freesurfer/subjects
# SearchSpace_mm2 65416.6
# SearchSpace_vtx 163842
# Bonferroni 0
# Minimum Threshold 3
# Maximum Threshold infinity
# Threshold Sign    pos
# AdjustThreshWhenOneTail 0
# Area Threshold    50 mm^2
# Overall max inf at vertex 603
# Overall min -1.07857 at vertex 50788
# NClusters          7
# Total Cortical Surface Area 65416.6 (mm^2)
# FixMNI = 1
# 
# ClusterNo  Max   VtxMax   Size(mm^2)  TalX   TalY   TalZ    NVtxs
   1          inf     603  43963.26    -22.0   -8.5  -26.0  85084
   2        7.417    1507    517.43     -6.8   20.0  -12.9  1085
   3        5.842   43374    112.35    -32.2  -21.7   16.6   335
   4        4.357   37577     71.15    -34.2   21.1  -15.4   149
   5        4.309   18511     53.87    -23.9  -76.6   26.1    80
   6        4.234  130755     70.09    -12.9  -83.8    9.7   126
   7        3.381   42201     67.58    -26.0  -83.1   15.8   102
