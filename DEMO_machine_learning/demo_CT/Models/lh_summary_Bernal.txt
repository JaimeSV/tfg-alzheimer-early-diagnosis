# Cluster Growing Summary (mri_surfcluster)
# $Id: mri_surfcluster.c,v 1.51.2.3 2012/05/31 22:10:05 greve Exp $
# $Id: mrisurf.c,v 1.693.2.7 2013/05/12 22:28:01 nicks Exp $
# CreationTime 2018/06/14-22:52:31-GMT
# cmdline mri_surfcluster --in Models/p_value_Bernal_LH.mgh --subject /fsaverage --hemi lh --sign pos --no-adjust --fdr 0.05 --minarea 50 --cwsig Clusters/FDR_0.05/lh_clusters_Bernal.mgh --sum Clusters/FDR_0.05/lh_summary_Bernal.txt --olab Clusters/FDR_0.05/lh_label_Bernal 
# cwd /home/jsimarro/Desktop/demoCT
# sysname  Linux
# hostname copernico
# machine  x86_64
# FixVertexAreaFlag 1
# FixSurfClusterArea 1
# 
# Input      Models/p_value_Bernal_LH.mgh
# Frame Number      0
# srcsubj /fsaverage
# hemi lh
# surface white
# SUBJECTS_DIR /usr/local/freesurfer/subjects
# FDR 0.050000
# SearchSpace_mm2 65416.6
# SearchSpace_vtx 163842
# Bonferroni 0
# Minimum Threshold 2.71959
# Maximum Threshold infinity
# Threshold Sign    pos
# AdjustThreshWhenOneTail 0
# Area Threshold    50 mm^2
# Overall max 2.67383 at vertex 161155
# Overall min -8.6673 at vertex 70671
# NClusters          0
# Total Cortical Surface Area 65416.6 (mm^2)
# FixMNI = 1
# 
# ClusterNo  Max   VtxMax   Size(mm^2)  TalX   TalY   TalZ    NVtxs
