function get_Thickness_mgh
% function get_Thickness_mgh(Type)
%% Initialize FreeSurfer in bash terminal
% sudo fdisk -l
% sudo mount /dev/sdc1 /media/Elements_
% export FREESURFER_HOME=/usr/local/freesurfer
% source /usr/local/freesurfer/SetUpFreeSurfer.sh
% export SUBJECTS_DIR=/media/Elements_/Copernico/Lin/Long
% export SUBJECTS_DIR=/media/Elements_/Newton/Sliebana/PacientesCompletados/Long
% export SUBJECTS_DIR=/media/Elements_/Gauss/sliebana/PacientesCompletados/Long
% export /home/jaime/Escritorio/TFG_V1/Analisis Masivo/demoCT


%%
% In this case the interest was in determining regionally specific 
% differences in cortical thickness atrophy rate over time among the previous 
% four groups of individuals: HC, sMCI, cMCI and AD. Thus we had the same 
% Qdec table of the previous example. The following Freesurfer's commands 
% were used to generate the spatial cortical thickness data:
% 
% 
% mris_preproc --qdec-long qdec.table.dat --target fsaverage --hemi lh --meas thickness --out lh.thickness.mgh
% 
% mri_surf2surf --hemi lh --s fsaverage --sval lh.thickness.mgh --tval lh.thickness_sm10.mgh --fwhm-trg 10 --cortex  --noreshape

%%
% SUBJECTS_DIR = '/media/Elements_/Copernico/Lin/Long'; %1103
% SUBJECTS_DIR = '/media/Elements_/Newton/Sliebana/PacientesCompletados/Long'; %174
% SUBJECTS_DIR = '/media/Elements_/Gauss/sliebana/PacientesCompletados/Long'; %107
% SUBJECTS_DIR = '/media/Elements_/Gauss/cplatero/Post/long'; %26 HSC
% SUBJECTS_DIR = '/media/Elements_/Gauss/cplatero/Post/long'; %26 HSC
SUBJECTS_DIR ='/media/Elements_/Copernico/cplatero/MCI/long';
cd(SUBJECTS_DIR);

name_dat='clinical_MCI490_1617.dat';
%qdecfile = '/home2/cplatero/hipocampo/maps/MCI_SC/174_MCI_SC.dat';    
%qdecfile = '/home2/cplatero/hipocampo/maps/NC_AD/174_NC_AD.dat';%Change
qdecfile = sprintf('/home2/cplatero/hipocampo/maps/%s',name_dat);
qdecOption = 'qdec-long';


% simboLink = sprintf('ln -s $FREESURFER_HOME/subjects/fsaverage6');
simboLink = sprintf('ln -s $FREESURFER_HOME/subjects/fsaverage');
fprintf(strcat(simboLink,'\n'));
system(simboLink);

% mris_preproc --qdec-long qdec.table.dat --target fsaverage --hemi lh --meas thickness --out lh.thickness.mgh 
% preproc = sprintf('mris_preproc --%s %s --target fsaverage6 --hemi lh --meas thickness --out lh.thickness.mgh',qdecOption,qdecfile);
preproc = sprintf('mris_preproc --%s %s --target fsaverage --hemi lh --meas thickness --out lh.thickness.mgh',qdecOption,qdecfile);
fprintf(strcat(preproc,'\n'));
system(preproc);

preproc = sprintf('mris_preproc --%s %s --target fsaverage --hemi rh --meas thickness --out rh.thickness.mgh',qdecOption,qdecfile);
fprintf(strcat(preproc,'\n'));
system(preproc);

% mri_surf2surf --hemi lh --s fsaverage --sval lh.thickness.mgh --tval lh.thickness_sm10.mgh --fwhm-trg 10 --cortex  --noreshape
% surf2surf = sprintf('mri_surf2surf --hemi lh --s fsaverage6 --sval lh.thickness.mgh --tval lh.1103_MCI_SC_sm10.mgh --fwhm-trg 10 --cortex  --noreshape');
% surf2surf = sprintf('mri_surf2surf --hemi lh --s fsaverage --sval lh.thickness.mgh --tval lh.174_MCI_SC_sm10_fs.mgh --fwhm-trg 10 --cortex  --noreshape');
% surf2surf = sprintf('mri_surf2surf --hemi lh --s fsaverage --sval lh.thickness.mgh --tval lh.174_NC_AD_sm10_fs.mgh --fwhm-trg 10 --cortex  --noreshape'); %Change
name_ct='MCI_1617_lin';
surf2surf = sprintf('mri_surf2surf --hemi lh --s fsaverage --sval lh.thickness.mgh --tval lh.%s_sm10_fs.mgh --fwhm-trg 10 --cortex  --noreshape',...
    name_ct); 
fprintf(strcat(surf2surf,'\n'));
system(surf2surf);

surf2surf = sprintf('mri_surf2surf --hemi rh --s fsaverage --sval rh.thickness.mgh --tval rh.%s_sm10_fs.mgh --fwhm-trg 10 --cortex  --noreshape',...
    name_ct); 
fprintf(strcat(surf2surf,'\n'));
system(surf2surf);

% system('unlink fsaverage6');
system('unlink fsaverage');



% lhsphere = fs_read_surf('/usr/local/freesurfer/subjects/fsaverage6/surf/lh.sphere');
% lhcortex = fs_read_label('/usr/local/freesurfer/subjects/fsaverage6/label/lh.cortex.label');
% save lh_sphere_cortex_label lhsphere lhcortex
end