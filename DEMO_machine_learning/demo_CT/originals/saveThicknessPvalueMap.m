function saveThicknessPvalueMap
% visulize *.mgh with freeview 

addpath('../../long/long_mixed_effects_matlab-tools/lme/');
[qval_lh,qval_rh,sign_lh,sign_rh]=getFDRMaps('Bernal');

path_MAT='../MCI_SC/Bernal/';
[~,mri_lh] = fs_read_Y(strcat(path_MAT,'lh.50sMCI_vs_50cMCI_long_thickness_sm10.mgh'));
[~,mri_rh] = fs_read_Y(strcat(path_MAT,'rh.50sMCI_vs_50cMCI_long_thickness_sm10.mgh'));

mri1_lh=mri_lh;
mri1_lh.volsz(4) = 1;

mri1_rh=mri_rh;
mri1_rh.volsz(4) = 1;

fs_write_Y(-log10(qval_lh).*sign_lh*(-1),mri1_lh,'q_map_SC_Bernal_LH.mgh');
fs_write_Y(-log10(qval_rh).*sign_rh*(-1),mri1_rh,'q_map_SC_Bernal_RH.mgh');

end
