function buildMassLmeModels
addpath('../../long/long_mixed_effects_matlab-tools/lme/');
addpath('../../long/long_mixed_effects_matlab-tools/lme/Qdec/');
addpath('../../long/long_mixed_effects_matlab-tools/lme/mass_univariate/');
addpath('../../long/long_mixed_effects_matlab-tools/lme/geodesic/');
addpath('../../long/long_mixed_effects_matlab-tools/lme/univariate/');
%% Read demographic and clinical data
%% 1617 data
% % load('534','Tresults','index_new_delete');
% load('668');
% 
% time_BS=Tresults.years;
% BlAge=Tresults.Age-time_BS;
% ni=get_ni(time_BS);
% gr_pMCI=double(Tresults.Convert);
% X=[ones(size(gr_pMCI,1),1),time_BS,gr_pMCI,time_BS.*gr_pMCI,BlAge];
% path_MAT='../MCI_SC/16_17/';
% 
% 
% [Y1_l,~] = fs_read_Y(strcat(path_MAT,'lh.MCI_1617_lin_sm10_fs.mgh'));
% [Y2_l,~] = fs_read_Y(strcat(path_MAT,'lh.MCI_1617_sliebana_sm10_fs.mgh'));
% 
% [Y1_r,~] = fs_read_Y(strcat(path_MAT,'rh.MCI_1617_lin_sm10_fs.mgh'));
% [Y2_r,~] = fs_read_Y(strcat(path_MAT,'rh.MCI_1617_sliebana_sm10_fs.mgh'));
% 
% Y_lh=[Y1_l;Y2_l];
% Y_rh=[Y1_r;Y2_r];
% Y_lh([406,516],:)=[];
% Y_rh([406,516],:)=[];
% 
% % % 534
% % feat_lh(index_new_delete,:)=[];
% % feat_rh(index_new_delete,:)=[];
%% Bernal data
path_MAT='../MCI_SC/Bernal/';
load(strcat(path_MAT,'ADNI_Long_50sMCI_vs_50cMCI'),'X','ni');
X=X(:,[1:4,8]);% X=[ones,time_scan,group,group*time,age_baseline]
[Y_lh,~] = fs_read_Y(strcat(path_MAT,'lh.50sMCI_vs_50cMCI_long_thickness_sm10.mgh'));
[Y_rh,~] = fs_read_Y(strcat(path_MAT,'rh.50sMCI_vs_50cMCI_long_thickness_sm10.mgh'));

%% auxiliar variables 
load('lh_sphere_cortex_label_fs','lhcortex','lhsphere');
load('rh_sphere_cortex_label_fs','rhcortex','rhsphere');

%% save ST-LME models
n_boot=8;


for i=1:n_boot
    clc;
    fprintf('Sample %d\n',i);
    [mask_scan_train,ni_two_train,ni_two_test]=splitSamples(ni,.75);
    
    
     
    %% Mass model save
    [lbClus_LH,model_LH,F_LH]=getMassLme(X(mask_scan_train,:),Y_lh(mask_scan_train,:),...
        ni_two_train,lhcortex,lhsphere);

    [lbClus_RH,model_RH,F_RH]=getMassLme(X(mask_scan_train,:),Y_rh(mask_scan_train,:),...
        ni_two_train,rhcortex,rhsphere);
    Bhat_LH=convertStructBhat(model_LH);
    Bhat_RH=convertStructBhat(model_RH);
    
    save(sprintf('mass_SC_Bernal_%d',i),'mask_scan_train','ni_two_train',...
        'ni_two_test','lbClus_LH','Bhat_LH','F_LH','lbClus_RH','Bhat_RH','F_RH');

      
end




end



function ni = get_ni(time_visit)
mask_baseline=time_visit==0;
numScans=length(time_visit);
ni=zeros(sum(mask_baseline),1);
j=1;
for i=1:length(ni)
    if(mask_baseline(j)==1 && j<numScans )
        j=j+1;
        k=1;
        while(time_visit(j)>0 && j<numScans)
            j=j+1;
            k=k+1;
        end
        ni(i)=k;
        if(j==numScans)
            ni(i)=k+1;
        end
    elseif(j==numScans)
        ni(i)=1;
    else
        warning('Error calculating ni');
    end
    
end

end


function [mask_scan_train,ni_two_train,ni_two_test]=splitSamples(ni_two,percent)
 numPatients=length(ni_two);
 p = randperm(numPatients);
 test=p(fix(percent*numPatients)+1:end);
 flag_train=true(numPatients,1);
 for i=1:length(test)
    flag_train(test(i))=false;
 end
 
 ni_two_train=zeros(sum(flag_train==true),1);
 ni_two_test=zeros(numPatients-sum(flag_train==true),1);
 j=1;
 k=1;
 numImg=1;
 mask_scan_train=true(sum(ni_two),1);
 for i=1:numPatients
     if(flag_train(i)==true)
        ni_two_train(j)= ni_two(i);
        j=j+1;
     else
        ni_two_test(k)= ni_two(i);
        k=k+1;
        mask_scan_train(numImg:numImg+ni_two(i)-1)=false;
     end
     numImg=numImg+ni_two(i);
 end
 
  
end

function [lhRgs,lhstats,F_lhstats]=getMassLme(X,Y,ni,hcortex,hsphere)

%% Initial vertex-wise temporal covariance estimates
[lhTh0,lhRe] = lme_mass_fit_EMinit(X,[1 2],Y,ni,hcortex,3);

% These covariance estimates were segmented into homogeneous regions using:
[lhRgs,lhRgMeans] = lme_mass_RgGrow(hsphere,lhRe,lhTh0,hcortex,2,95);

% Here both lhTh0 and lhRgMeans maps were overlaid onto lhsphere and visually
% compared each other to ensure that they were similar enough 
% (the essential spatial organization of the initial covariance estimates 
% was not lost after the segmentation procedure, otherwise the above input 
% value 2 must be reduced to 1.8 and so on):

% surf.faces =  lhsphere.tri;
% surf.vertices =  lhsphere.coord';
% 
% figure; p1 = patch(surf);
% set(p1,'facecolor','interp','edgecolor','none','facevertexcdata',lhTh0(1,:)');
% 
% figure; p2 = patch(surf); 
% set(p2,'facecolor','interp','edgecolor','none','facevertexcdata',lhRgMeans(1,:)');

% The spatiotemporal LME model was fitted using:
lhstats = lme_mass_fit_Rgw(X,[1 2],Y,ni,lhTh0,lhRgs,hsphere);


% CM.C = [0 0 1 0 0 0 0 0];
CM.C = [0 0 1 0 0];
F_lhstats = lme_mass_F(lhstats,CM);

end



function Bhat=convertStructBhat(lhstats)
Bhat=zeros(size(lhstats(1).Bhat,1),size(lhstats,2));
for i=1:size(lhstats,2)
    if(isempty(lhstats(i).Bhat)==0)
        Bhat(:,i)=lhstats(i).Bhat;
    end
end
end
