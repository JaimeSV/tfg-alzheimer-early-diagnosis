function [err_I,err_q]=controlTypeI_mass
addpath('../../long/long_mixed_effects_matlab-tools/lme/');
addpath('../../long/long_mixed_effects_matlab-tools/lme/Qdec/');
addpath('../../long/long_mixed_effects_matlab-tools/lme/mass_univariate/');


%% load NC vs NC ST-LME Models
path_MAT='../NC_NC/';
listMAT=dir(strcat(path_MAT,'mass_NC_NC_*.mat'));
n_boot=numel(listMAT);


alpha_th=[1e-3,1e-2,.05,.1,.2];
err_I=zeros(n_boot,2*length(alpha_th));

% numVoxels=163842;
numVoxels=40962; %fsaverage6

pvalue_lh=zeros(numVoxels,n_boot);
pvalue_rh=zeros(numVoxels,n_boot);
sgn_lh=zeros(numVoxels,n_boot);
sgn_rh=zeros(numVoxels,n_boot);

for i=1:n_boot
    clc;
    fprintf('Sample %d\n',i);
    %% Type error
    load(strcat(path_MAT,listMAT(i).name),'F_LH','F_RH');
    err_I(i,:)=[getTypeI(F_LH.pval,alpha_th);getTypeI(F_RH.pval,alpha_th)]';
    
    pvalue_lh(:,i)=F_LH.pval;
    pvalue_rh(:,i)=F_RH.pval;
    sgn_lh(:,i)=F_LH.sgn;
    sgn_rh(:,i)=F_RH.sgn;
    
end

[detvtx_lh,sided_pval_lh,pth_lh,m0_lh] = lme_mass_FDR2(pvalue_lh,sgn_lh,[],0.05,0);
[detvtx_rh,sided_pval_rh,pth_rh,m0_rh] = lme_mass_FDR2(pvalue_rh,sgn_rh,[],0.05,0);
err_q=[getTypeI(sided_pval_lh',alpha_th);getTypeI(sided_pval_rh',alpha_th)]';


end



function err_I = getTypeI(pval,alpha_th)

numErr=length(alpha_th);
samples=length(pval);
err_I=zeros(numErr,1);
for i=1:numErr
    err_I(i)=sum(pval<alpha_th(i))/samples;    
end

end
