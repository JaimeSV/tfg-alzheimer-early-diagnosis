function [qval_lh,qval_rh,sign_lh,sign_rh]=getFDRMaps(twoGroups)

 addpath(genpath('lme/'));


%% Classification using ROI-based markers
if(strcmp(twoGroups,'Bernal'))
    path_MAT='Models/';    
    listMAT=dir(strcat(path_MAT,'mass_SC_Bernal_*.mat'));

   
% elseif(strcmp(twoGroups,'16_17'))   
%     path_MAT='../MCI_SC/16_17/';
%     % listMAT=dir(strcat(path_MAT,'mass_SC1617_AD_FS_LR_*.mat'));
%     listMAT=dir(strcat(path_MAT,'mass_SC_FS_LR_*.mat'));
    
elseif(strcmp(twoGroups,'NC_AD'))   
    path_MAT='Models/';    
    listMAT=dir(strcat(path_MAT,'mass_NC_AD_FS_LR_*.mat'));
    
%     else

end


n_boot=numel(listMAT);

% numVoxels=40962;%fsaverage6
numVoxels=163842; %fsaverage

pvalue_lh=zeros(numVoxels,n_boot);
pvalue_rh=zeros(numVoxels,n_boot);
sgn_lh=zeros(numVoxels,n_boot);
sgn_rh=zeros(numVoxels,n_boot);

for i=1:n_boot
    clc;
    fprintf('Sample %d\n',i);
    load(strcat(path_MAT,listMAT(i).name),'F_LH','F_RH');
    
   
    %% Mass
    pvalue_lh(:,i)=F_LH.pval;
    sgn_lh(:,i)=F_LH.sgn;
    
    pvalue_rh(:,i)=F_RH.pval;
    sgn_rh(:,i)=F_RH.sgn;
 
end
[~,qval_lh,pth_lh,m0_lh] = lme_mass_FDR2(pvalue_lh,sgn_lh,[],0.05,0);
qval_lh=qval_lh';
sign_lh=sum(sgn_lh,2)/n_boot;

[~,qval_rh,pth_rh,m0_rh] = lme_mass_FDR2(pvalue_rh,sgn_rh,[],0.05,0);
qval_rh=qval_rh';
sign_rh=sum(sgn_rh,2)/n_boot;

% rmpath('lme');


end

