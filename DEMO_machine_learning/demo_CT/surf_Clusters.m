function surf_Clusters()
% BIBLIOGRAPHY https://surfer.nmr.mgh.harvard.edu/fswiki/mri_surfcluster
% This tool allows you to cluster surface data.
% This program assigns each vertex on a cortical surface to a cluster based 
% on the distribution of intensity values in the source file. 
% A vertex must meet two criteria to be part of a cluster. 
% First, its intensity must fall within a certain range (the intensity threshold criteria). 
% Second, it must be part of a contiguous set of vertices that meet the threshold criteria 
% and whose surface area is greater than a minimum

path=' Clusters/qvalue_NC_AD/thmin_8';
% Right hemisphere
system(strcat('mri_surfcluster',...
    ' --in Models/q_map_NC_AD_RH.mgh',... %Input file
    ' --subject /fsaverage',... % Surface values are defined on this subject.
    ' --hemi rh',... % Specify the cortical hemisphere that the input represents
    ' --thmin 8',... % Minimum threshold in the intensity threshold criteria. Vertices on
    ' --sign pos ',... %This is used to control the sign of the threshold criteria
   ' --no-adjust',...% Dont assme that pvalue was computed using a two-sided t-test.
    ' --minarea 50 ',... %Minimum surface area (in mm^2) that a set of contiguous vertices must achieve in order to be considered a cluster. 50 mm^2
    strcat(' --cwsig ',path,'rh_clusters_NC_AD.mgh'),... %Saves the sigificance map of the clusters
     strcat(' --sum ',path,'rh_summary_NC_AD.txt'),... %The summary text file
     strcat(' --olab ',path,'rh_label_NC_AD'))); %Save output clusters as labels.

% Left hemisphere
system(strcat('mri_surfcluster',...
    ' --in Models/q_map_NC_AD_LH.mgh',...
    ' --subject /fsaverage',...
    ' --hemi lh',...
    ' --thmin 8',...
    ' --sign pos',...
    ' --no-adjust',...
    ' --minarea 50' ,...
     strcat(' --cwsig ',path,'lh_clusters_NC_AD.mgh'),...
     strcat(' --sum ',path,'lh_summary_NC_AD.txt'),...
     strcat(' --olab ',path,'lh_label_NC_AD')));


end
