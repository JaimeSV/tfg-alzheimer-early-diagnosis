function saveThicknessPvalueMap
% visulize *.mgh with freeview 
% cd /usr/local/freesurfer/bin
% export FREESURFER_HOME=/usr/local/freesurfer
% source /usr/local/freesurfer/SetUpFreeSurfer.sh
% freeview
% sourfaces, load fsaverage rh.pial
% overlay cluster
% anotation ls aparc annot ->Atlas  fsaverage label aparc.annot
% edit preferences background color
addpath(genpath('lme'));
%[qval_lh,qval_rh,sign_lh,sign_rh]=getFDRMaps('Bernal');
 [qval_lh,qval_rh,sign_lh,sign_rh]=getFDRMaps('NC_AD');

path_MAT='Data_Bernal/mass_univariate/';
[~,mri_lh] = fs_read_Y(strcat(path_MAT,'lh.50sMCI_vs_50cMCI_long_thickness_sm10.mgh'));
[~,mri_rh] = fs_read_Y(strcat(path_MAT,'rh.50sMCI_vs_50cMCI_long_thickness_sm10.mgh'));

mri1_lh=mri_lh;
mri1_lh.volsz(4) = 1; %n de modelos

mri1_rh=mri_rh;
mri1_rh.volsz(4) = 1;

fs_write_Y(-log10(qval_lh).*sign_lh*(-1),mri1_lh,'Models/q_map_NC_AD_LH.mgh');
fs_write_Y(-log10(qval_rh).*sign_rh*(-1),mri1_rh,'Models/q_map_NC_AD_RH.mgh');
rmpath('lme');
end
