%% Flow-chart
% 1. get_Thickness_mgh
% 2. buildMassLmeModels
% 3. saveThicknessPvalueMap (visualize freeview)
% 4. ST-LME power (NC vs AD)
% 5. controlTypeI error (NC vs NC)

% 
% https://surfer.nmr.mgh.harvard.edu/fswiki/mri_surfcluster
% join
%% Function
%get_Thickness_mgh: read cortical thickness from Freesurfer longitudinal framework
%buildMassLmeModels: build ST-LME Models from *.mgh and clinical data
%getFDRMaps       : calculates q-value
%saveThicknessPvalueMap: save q-value into *.mgh (visualization)
%sensitivitySTLME: calculate ST-LME power (see Bernal13b fig.1)
%controlTypeI_mass: error type I ST-LME (see Bernal13b table 2)
