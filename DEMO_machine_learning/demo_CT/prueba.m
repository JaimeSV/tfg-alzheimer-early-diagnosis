% fs_write_fstats(F_LH,'rh.50sMCI_vs_50cMCI_long_thickness_sm10','prueba','sig')
clear
addpath(genpath('lme'));
twoGroups='Bernal';
twoGroups='NC_AD';
if(strcmp(twoGroups,'Bernal'))
    path_MAT='Models/';    
    listMAT=dir(strcat(path_MAT,'mass_SC_Bernal_*.mat'));

elseif(strcmp(twoGroups,'NC_AD'))   
    path_MAT='Models/';    
    listMAT=dir(strcat(path_MAT,'mass_NC_AD_FS_LR_*.mat'));
    
%     else

end
n_boot=numel(listMAT);

% numVoxels=40962;%fsaverage6
numVoxels=163842; %fsaverage

pvalue_lh=zeros(numVoxels,n_boot);
pvalue_rh=zeros(numVoxels,n_boot);
sgn_lh=zeros(numVoxels,n_boot);
sgn_rh=zeros(numVoxels,n_boot);

for i=1:n_boot
    clc;
    fprintf('Sample %d\n',i);
    load(strcat(path_MAT,listMAT(i).name),'F_LH','F_RH');
    
   
    %% Mass
    pvalue_lh(:,i)=F_LH.pval;
    sgn_lh(:,i)=F_LH.sgn;
    
    pvalue_rh(:,i)=F_RH.pval;
    sgn_rh(:,i)=F_RH.sgn;
end

path_MAT='Data_Bernal/mass_univariate/';
[~,mri_lh] = fs_read_Y(strcat(path_MAT,'lh.50sMCI_vs_50cMCI_long_thickness_sm10.mgh'));
[~,mri_rh] = fs_read_Y(strcat(path_MAT,'rh.50sMCI_vs_50cMCI_long_thickness_sm10.mgh'));
 [a,mri_lh] = fs_read_Y(strcat('Models/p_value_NC_AD_LH.mgh'));
 [a,mri_lh] = fs_read_Y(strcat('Models/q_map_NC_AD_LH.mgh'));

mri1_lh=mri_lh;
mri1_lh.volsz(4) = n_boot; %n de modelos

mri1_rh=mri_rh;
mri1_rh.volsz(4) = n_boot;

% [~,mri_rh_AA] = fs_read_Y(strcat('Data_thickness/','rh.MCI_1617_lin_sm10_fs.mgh'));
% mri_rh_AA.volsz(4) = n_boot;
% isequal(mri1_rh,mri_rh_AA)


    fs_write_Y(-log10(pvalue_lh).*sgn_lh,mri1_lh,strcat('Models/p_value_',twoGroups,'_LH.mgh'));
        fs_write_Y(-log10(pvalue_rh).*sgn_rh,mri1_rh,strcat('Models/p_value_',twoGroups,'_RH.mgh'));

