function [power,power_lh,power_rh]=sensitivitySTLME

addpath(genpath('lme/'));


%% load  NC vs AD ST-LME models 
path_MAT='Models/';
 listMAT=dir(strcat(path_MAT,'mass_NC_AD_FS_LR_*.mat'));
%listMAT=dir(strcat(path_MAT,'mass_SC_Bernal_*.mat'));

n_boot=numel(listMAT);

% numVoxels=40962;
numVoxels=163842;

pvalue_lh=zeros(numVoxels,n_boot);
pvalue_rh=zeros(numVoxels,n_boot);
sgn_lh=zeros(numVoxels,n_boot);
sgn_rh=zeros(numVoxels,n_boot);

for i=1:n_boot
    clc;
    fprintf('Sample %d\n',i);
    load(strcat(path_MAT,listMAT(i).name),'F_LH','F_RH');
    
   
    %% Mass
    pvalue_lh(:,i)=F_LH.pval;
    sgn_lh(:,i)=F_LH.sgn;
    
    pvalue_rh(:,i)=F_RH.pval;
    sgn_rh(:,i)=F_RH.sgn;
 
end

alpha_th=[1e-3,1e-2,.05,.1,.2];
power_lh=controlTypeII(pvalue_lh,sgn_lh,alpha_th);
power_rh=controlTypeII(pvalue_rh,sgn_rh,alpha_th);
power=mean([power_lh';power_rh']);

end


function power=controlTypeII(pvalue,sgn,alpha_th)
power=zeros(length(alpha_th),1);
numVertex=size(pvalue,1);
for i=1:length(alpha_th)
    %[~,~,~,m0] = lme_mass_FDR2(pvalue,sgn,[],alpha_th(i),0);
    m0=mean(sum(pvalue>alpha_th(i)));
    power(i)=(numVertex-m0)/numVertex;
end
    
end
