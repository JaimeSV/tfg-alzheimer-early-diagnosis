clc
clear
addpath(genpath('../lme/'));


if 1 %1718 MCI with cluster from MCI
old_1 = readtable('../Data_all/MCI_1718/clinical_Jaime_1718.dat');
old_2 = readtable('../Data_all/MCI_1718/clinical_MCI_jrojo_1718.dat');
old_3 = readtable('../Data_all/MCI_1718/clinical_sMCI_jwu.dat');

load('../Data_all/17_18.mat','Tresults');
% out_Table = '../Data_all/17_18_with_clusters.mat';
cluster_path='Clusters/qvalue_Bernal/thmin_2';
%% Making tables
T1_rh = extract_cluster_table (...
    '../Data_all/MCI_1718/rh.MCI_1718_jsimarro_sm10_fs.mgh',...
    strcat(cluster_path,'rh_clusters_Bernal.mgh'),'rh_MCI');
T1_lh = extract_cluster_table (...
    '../Data_all/MCI_1718/lh.MCI_1718_jsimarro_sm10_fs.mgh',...
    strcat(cluster_path,'lh_clusters_Bernal.mgh'),'lh_MCI');
T1 =[T1_rh,T1_lh];
T1.fsid = old_1.fsid;

T2_rh = extract_cluster_table (...
    '../Data_all/MCI_1718/rh.MCI_jrojo_1718_sm10_fs.mgh',...
    strcat(cluster_path,'rh_clusters_Bernal.mgh'),'rh_MCI');
T2_lh = extract_cluster_table (...
    '../Data_all/MCI_1718/lh.MCI_jrojo_1718_sm10_fs.mgh',...
    strcat(cluster_path,'lh_clusters_Bernal.mgh'),'lh_MCI');
T2 =[T2_rh,T2_lh];
T2.fsid = old_2.fsid;

T3_rh = extract_cluster_table (...
    '../Data_all/MCI_1718/rh.sMCI_1718_jwu_sm10_fs.mgh',...
    strcat(cluster_path,'rh_clusters_Bernal.mgh'),'rh_MCI');
T3_lh = extract_cluster_table (...
    '../Data_all/MCI_1718/lh.sMCI_1718_jwu_sm10_fs.mgh',...
    strcat(cluster_path,'lh_clusters_Bernal.mgh'),'lh_MCI');
T3 =[T3_rh,T3_lh];
T3.fsid = old_3.fsid;
Tmass1 = [T1;T2;T3];
end

if 1 % 1718 MCI with cluster from AD
old_1 = readtable('../Data_all/MCI_1718/clinical_Jaime_1718.dat');
old_2 = readtable('../Data_all/MCI_1718/clinical_MCI_jrojo_1718.dat');
old_3 = readtable('../Data_all/MCI_1718/clinical_sMCI_jwu.dat');

load('../Data_all/17_18.mat','Tresults');
cluster_path='Clusters/qvalue_NC_AD/';
%% Making tables
T1_rh = extract_cluster_table (...
    '../Data_all/MCI_1718/rh.MCI_1718_jsimarro_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8rh_clusters_NC_AD.mgh'),'rh_AD');
T1_lh = extract_cluster_table (...
    '../Data_all/MCI_1718/lh.MCI_1718_jsimarro_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8lh_clusters_NC_AD.mgh'),'lh_AD');
T1 =[T1_rh,T1_lh];

T2_rh = extract_cluster_table (...
    '../Data_all/MCI_1718/rh.MCI_jrojo_1718_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8rh_clusters_NC_AD.mgh'),'rh_AD');
T2_lh = extract_cluster_table (...
    '../Data_all/MCI_1718/lh.MCI_jrojo_1718_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8lh_clusters_NC_AD.mgh'),'lh_AD');
T2 =[T2_rh,T2_lh];

T3_rh = extract_cluster_table (...
    '../Data_all/MCI_1718/rh.sMCI_1718_jwu_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8rh_clusters_NC_AD.mgh'),'rh_AD');
T3_lh = extract_cluster_table (...
    '../Data_all/MCI_1718/lh.sMCI_1718_jwu_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8lh_clusters_NC_AD.mgh'),'lh_AD');
T3 =[T3_rh,T3_lh];
Tmass2 = [T1;T2;T3];
end

TmassA=[Tmass1,Tmass2];


if 1 % 1617 MCI with cluster from MCI
old_lin = readtable('../Data_all/MCI_1617/clinical_MCI_lin_1617.dat');
old_sliebana = readtable('../Data_all/MCI_1617/clinical_MCI_sliebana_1617.dat');
load('../Data_all/668_Jaime.mat','Tresults');
% out_Table = '../Data_all/668_Jaime.mat';
cluster_path='Clusters/qvalue_Bernal/thmin_2';
%% Making tables
% Table of LIN
Tlin_rh = extract_cluster_table (...
    '../Data_all/MCI_1617/rh.MCI_1617_lin_sm10_fs.mgh',...
    strcat(cluster_path,'rh_clusters_Bernal.mgh'),'rh_MCI');
Tlin_lh = extract_cluster_table (...
    '../Data_all/MCI_1617/lh.MCI_1617_lin_sm10_fs.mgh',...
    strcat(cluster_path,'lh_clusters_Bernal.mgh'),'lh_MCI');
Tlin =[Tlin_rh,Tlin_lh];
Tlin.fsid = old_lin.fsid;

% Table of SERGIO
Ts_rh = extract_cluster_table (...
    '../Data_all/MCI_1617/rh.MCI_1617_sliebana_sm10_fs.mgh',...
    strcat(cluster_path,'rh_clusters_Bernal.mgh'),'rh_MCI');
Ts_lh = extract_cluster_table (...
    '../Data_all/MCI_1617/lh.MCI_1617_sliebana_sm10_fs.mgh',...
    strcat(cluster_path,'lh_clusters_Bernal.mgh'),'lh_MCI');
Ts =[Ts_rh,Ts_lh];
Ts.fsid = old_sliebana.fsid;

Tmass1 = [Tlin;Ts];
end

if 1 %MCI with cluster from AD
old_lin = readtable('../Data_all/MCI_1617/clinical_MCI_lin_1617.dat');
old_sliebana = readtable('../Data_all/MCI_1617/clinical_MCI_sliebana_1617.dat');
load('../Data_all/668_Jaime.mat','Tresults');
% out_Table = '../Data/668_Jaime_with_cluster_from_AD.mat';
out_Table = '../Data_all/668_Jaime_with_cluster_from_both.mat';
cluster_path='Clusters/qvalue_NC_AD/';
%% Making tables
% Table of LIN
Tlin_rh = extract_cluster_table (...
    '../Data_all/MCI_1617/rh.MCI_1617_lin_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8rh_clusters_NC_AD.mgh'),'rh_AD');
Tlin_lh = extract_cluster_table (...
    '../Data_all/MCI_1617/lh.MCI_1617_lin_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8lh_clusters_NC_AD.mgh'),'lh_AD');
Tlin =[Tlin_rh,Tlin_lh];
% Tlin.fsid = old_lin.fsid;

% Table of SERGIO
Ts_rh = extract_cluster_table (...
    '../Data_all/MCI_1617/rh.MCI_1617_sliebana_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8rh_clusters_NC_AD.mgh'),'rh_AD');
Ts_lh = extract_cluster_table (...
    '../Data_all/MCI_1617/lh.MCI_1617_sliebana_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8lh_clusters_NC_AD.mgh'),'lh_AD');
Ts =[Ts_rh,Ts_lh];
% Ts.fsid = old_sliebana.fsid;

%% Join Mass result with RoiTest table
Tmass2 = [Tlin;Ts];
end
TmassB=[Tmass1,Tmass2];


if 0%  AD with cluster from AD
old_107 = readtable('../Data/NC_AD/107_NC_AD.dat');
old_174 = readtable('../Data/NC_AD/174_NC_AD.dat');
old_1103 = readtable('../Data/NC_AD/1103_NC_AD.dat');

load('../Data/NC_AD.mat','Tresults');
out_Table = '../Data/NC_AD_with_cluster_from_both.mat';

cluster_path='Clusters/qvalue_NC_AD/';
%% Making tables
% Table of 107
T1_rh = extract_cluster_table (...
    '../Data/NC_AD/rh.107_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8rh_clusters_NC_AD.mgh'),'rh_AD');
T1_lh = extract_cluster_table (...
    '../Data/NC_AD/lh.107_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8lh_clusters_NC_AD.mgh'),'lh_AD');
T1 =[T1_rh,T1_lh];
T1.fsid = old_107.fsid;
% Table of 174
T2_rh = extract_cluster_table (...
    '../Data/NC_AD/rh.174_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8rh_clusters_NC_AD.mgh'),'rh_AD');
T2_lh = extract_cluster_table (...
    '../Data/NC_AD/lh.174_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8lh_clusters_NC_AD.mgh'),'lh_AD');
T2 =[T2_rh,T2_lh];
T2.fsid = old_174.fsid;
% Table of 1103
T3_rh = extract_cluster_table (...
    '../Data/NC_AD/rh.1103_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8rh_clusters_NC_AD.mgh'),'rh_AD');
T3_lh = extract_cluster_table (...
    '../Data/NC_AD/lh.1103_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'thmin_8lh_clusters_NC_AD.mgh'),'lh_AD');
T3 =[T3_rh,T3_lh];
T3.fsid = old_1103.fsid;

Tmass1 = [T1;T2;T3];

end

if 0 %AD from MCI
    cluster_path='Clusters/qvalue_Bernal/thmin_2';

% Table of 107
T1_rh = extract_cluster_table (...
    '../Data/NC_AD/rh.107_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'rh_clusters_Bernal.mgh'),'rh_MCI');
T1_lh = extract_cluster_table (...
    '../Data/NC_AD/lh.107_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'lh_clusters_Bernal.mgh'),'lh_MCI');
T1 =[T1_rh,T1_lh];
% T1.fsid = old_107.fsid;
% Table of 174
T2_rh = extract_cluster_table (...
    '../Data/NC_AD/rh.174_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'rh_clusters_Bernal.mgh'),'rh_MCI');
T2_lh = extract_cluster_table (...
    '../Data/NC_AD/lh.174_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'lh_clusters_Bernal.mgh'),'lh_MCI');
T2 =[T2_rh,T2_lh];
% T2.fsid = old_174.fsid;
% Table of 1103
T3_rh = extract_cluster_table (...
    '../Data/NC_AD/rh.1103_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'rh_clusters_Bernal.mgh'),'rh_MCI');
T3_lh = extract_cluster_table (...
    '../Data/NC_AD/lh.1103_NC_AD_sm10_fs.mgh',...
    strcat(cluster_path,'lh_clusters_Bernal.mgh'),'lh_MCI');
T3 =[T3_rh,T3_lh];
% T3.fsid = old_1103.fsid;
Tmass2 = [T1;T2;T3];

end
% Wrong:  346 010_S_0422 m30
% Wrong:  1068 067_S_0077 m30
% Wrong:  1178 128_S_1407 m12

Tmass=[TmassA;TmassB];
Tmass([346,1068,1178],:)=[];
% out_Table = '../Data_all/all_sMCI_pMCI_with_clusters.mat';
load('../Data_all/all_sMCI_pMCI')

[~, ind] = unique(Tmass.fsid);
% Detect duplicate indices
duplicate_ind = setdiff(1:size(Tmass.fsid, 1), ind);
Tmass(duplicate_ind,:)=[];
Tresults(duplicate_ind,:)=[];
% Tresults.fsid(duplicate_ind)
% isequal(Tmass(duplicate_ind(1:4),:),Tmass([539:543],:))
% isequal(Tresults(duplicate_ind(1:4),:),Tresults([539,540,541,543],:))
% isequal(Tmass(duplicate_ind(5:end),:),Tmass([1304:1307],:))
% isequal(Tresults(duplicate_ind(5:end),:),Tresults([1304:1307],:))


if ~isequal(Tmass.fsid, Tresults.fsid)
error('Error!!')
end
% Tmass=TmassA;
% Tmass(346,:)=[];
%  out_Table = '../Data_all/17_18_with_clusters.mat';
% load('../Data_all/17_18')

Tresults = join(Tresults,Tmass);
save(out_Table,'Tresults');

