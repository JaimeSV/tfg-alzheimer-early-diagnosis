function [err_I,err_q]=controlTypeI_mass
addpath(genpath('lme'));
addpath(genpath('export_fig'));

close all

%% load NC vs NC ST-LME Models
path_MAT='Models/';
listMAT=dir(strcat(path_MAT,'mass_SC_Bernal_*.mat'));
name='controlTypeI_Bernal';
p=0.01;
% listMAT=dir(strcat(path_MAT,'mass_NC_AD_FS_LR_*.mat'));
% name='controlTypeI_AD';
% p=0.00000001;

n_boot=numel(listMAT);


alpha_th=[1e-3,1e-2,.05,.1,.2];
err_I=zeros(n_boot,2*length(alpha_th));

 numVoxels=163842;
%numVoxels=40962; %fsaverage6

pvalue_lh=zeros(numVoxels,n_boot);
pvalue_rh=zeros(numVoxels,n_boot);
sgn_lh=zeros(numVoxels,n_boot);
sgn_rh=zeros(numVoxels,n_boot);

for i=1:n_boot
    clc;
    fprintf('Sample %d\n',i);
    %% Type error
    load(strcat(path_MAT,listMAT(i).name),'F_LH','F_RH');
    err_I(i,:)=[getTypeI(F_LH.pval,alpha_th);getTypeI(F_RH.pval,alpha_th)]';
    err_I_lh(i,:)=getTypeI(F_LH.pval,alpha_th);
    err_I_rh(i,:)=getTypeI(F_RH.pval,alpha_th)';

    pvalue_lh(:,i)=F_LH.pval;
    pvalue_rh(:,i)=F_RH.pval;
    sgn_lh(:,i)=F_LH.sgn;
    sgn_rh(:,i)=F_RH.sgn;
    
end

[detvtx_lh,sided_pval_lh,pth_lh,m0_lh] = lme_mass_FDR2(pvalue_lh,sgn_lh,[],p,0);
[detvtx_rh,sided_pval_rh,pth_rh,m0_rh] = lme_mass_FDR2(pvalue_rh,sgn_rh,[],p,0);
err_q=[getTypeI(sided_pval_lh',alpha_th);getTypeI(sided_pval_rh',alpha_th)]';
err_q_lh=getTypeI(sided_pval_lh',alpha_th);
err_q_rh=getTypeI(sided_pval_rh',alpha_th);


 f1=figure('Position',[2 200 1500 500]);

subplot(1,2,1)
boxplot(err_I_lh,'labels',{'0.001','0.01','0.05','0.10','0.20'})
ylabel('Error de tipo I','FontSize',15)
xlabel('$\alpha$','Interpreter','latex','FontSize',20);
hold on
title('Hemisferio izquierdo')

plot(err_q_lh,'o','MarkerSize' ,10)
hold off
box off;

subplot(1,2,2)
boxplot(err_I_lh,'labels',{'0.001','0.01','0.05','0.10','0.20'})
ylabel('Error de tipo I','FontSize',15)
xlabel('$\alpha$','Interpreter','latex','FontSize',20);
hold on
title('Hemisferio derecho')

plot(err_q_rh,'o','MarkerSize' ,10)
hold off
box off;
save_fig(name)

end



function err_I = getTypeI(pval,alpha_th)

numErr=length(alpha_th);
samples=length(pval);
err_I=zeros(numErr,1);
for i=1:numErr
    err_I(i)=sum(pval<alpha_th(i))/samples;    
end

end

function [] =save_fig(name)
box off;
name_path=strcat('/home/jaime/Escritorio/TFG_V4/Documento_TFG/figuras/resultados/',name,'.pdf');
set(gcf,'color','white')
export_fig(name_path);    
end