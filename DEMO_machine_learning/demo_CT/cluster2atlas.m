function cluster2atlas

addpath(genpath('../lme'));
addpath(genpath('../fsfast'));

%% data MCI 17/18 jrojo
% [lh_clusters,~] = fs_read_Y('Clusters/qvalue_Bernal/thmin_2lh_clusters_Bernal.mgh');
% [rh_clusters,~] = fs_read_Y('Clusters/qvalue_Bernal/thmin_2rh_clusters_Bernal.mgh');

[lh_clusters,~] = fs_read_Y('Clusters/qvalue_NC_AD/thmin_8lh_clusters_NC_AD.mgh');
[rh_clusters,~] = fs_read_Y('Clusters/qvalue_NC_AD/thmin_8rh_clusters_NC_AD.mgh');

lh_names=clusterlabelling(lh_clusters(4,:)','lh');
rh_names=clusterlabelling(rh_clusters(4,:)','rh');

%fprintf('\n Lh-Cluster %i',i)
for i=1:size(lh_names,1)
   %fprintf('\n lh\\_MCI\\_cl\\_%i &',i)
       fprintf('\n lh\\_AD\\_cl\\_%i &',i)

    for j=1:size(lh_names,2)
        fprintf(' %s',lh_names{i,j})
        if j<size(lh_names,2)
            if ~isempty(lh_names{i,j+1})
                fprintf(',\t ')
            end
        end
    end
    fprintf(' \\\\ ')
    
end
fprintf(' \n \\hline')

for i=1:size(rh_names,1)
  %  fprintf('\n rh\\_MCI\\_cl\\_%i &',i)
        fprintf('\n rh\\_AD\\_cl\\_%i &',i)

    for j=1:size(rh_names,2)
        
        fprintf(' %s',rh_names{i,j})
        if j<size(rh_names,2)
            if ~isempty(rh_names{i,j+1})
                fprintf(',\t ')
            end
        end
    end
    fprintf(' \\\\ ')
    
end
fprintf('\n')

end



function h_names=clusterlabelling(h_clusters,hemi)

if(strcmp(hemi,'lh'))
    % [~,label_h,color_h]=read_annotation('Clusters/lh.aparc.a2009s.annot');
    [~,label_h,color_h]=read_annotation('Clusters/lh.aparc.annot');
    
else
    %     [~,label_h,color_h]=read_annotation('Clusters/rh.aparc.a2009s.annot');
    [~,label_h,color_h]=read_annotation('Clusters/rh.aparc.annot');
    
end

clusterindex = unique(h_clusters);
clusterindex = clusterindex(2:end);

table_h=color_h.table(:,5);
h_names = cell(length(clusterindex),6);

for i=1:length(clusterindex)
    mask=h_clusters==clusterindex(i);
    anatomicalROIs=unique(label_h(mask));
    for j=1:length(anatomicalROIs)
        index=find(table_h==anatomicalROIs(j));
        h_names{i,j} = color_h.struct_names{index};
    end
end

end
