function clusterTable = extract_cluster_table (populationfile,clusterfile, hemi)
    
    population_thickness = fs_read_Y(populationfile);    
    clusterinfo = fs_read_Y(clusterfile);%The 4th row contains the label for all

    
    % Originally Clusterindex will have a 0 label that corresponds to all
    % vertex that were not included in any original label.
    clusterindex = unique(clusterinfo(4,:));
    clusterindex(find(clusterindex==0))=[];
    
    cluster_names = cell(1,numel(clusterindex));
    cluster_values = zeros(size(population_thickness,1),numel(clusterindex));
    
    for i=1:numel(clusterindex)
        cluster_names{i} = strcat(hemi,'_cl_',num2str(i));
        vertex_selection = find(clusterinfo(4,:) ==i);
        cluster_values(:,i)=mean(population_thickness(:,vertex_selection),2); %Mean of each clusten (column) in each subject (row)
    end
    
    clusterTable= array2table(cluster_values, 'VariableNames', cluster_names);
    
end