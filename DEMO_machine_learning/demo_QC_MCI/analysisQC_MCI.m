function analysisQC_MCI
close all;
addpath(genpath('../lme')); %lme_lowessPlot
addpath('export_fig')
%% data
%load('142');
 load('Jaime_V1');
 %pMCI
 
  gr_check=double(Tresults.Convert);
 mask_MCI_Check= gr_check==1;

time_BS_Check=Tresults.years(mask_MCI_Check);
ni_Check=get_ni(time_BS_Check,numel(unique(Tresults.fsidbase)));
BlAge=Tresults.Age(mask_MCI_Check)-time_BS_Check;
index_delete=[]
sum=1;
for i=1:size(ni_Check,1)
    if i==5 || i==20 ||  i==21 || i==22 % ni_Check(i)==6
        index_delete=[index_delete,(sum:sum+5)];
    end
        sum=sum+ni_Check(i);
end
Tresults(index_delete,:)=[];
%  Tresults2=Tresults;
%  load('668');
%  Tresults3=Tresults;
% load('RoiTest');
% Tresults=[Tresults2;Tresults;Tresults3];
%  Tresults([96,106],:)=[];
% ni_Check([5,20,21,22])=[];
% NHV_Check(index_delete,:)=[];



figure('Position',[2 200 1000 500]);
corrAsegADNI_QC(Tresults)
save_fig('QC_correlacion_volumetry')

%Checking QC
 load('QC_Jaime_long.mat','corr_QC','vol_QC');
QC=[corr_QC.coefImg_Hipp,corr_QC.coefLabel_Hipp,corr_QC.coefImg_Cortex,...
    corr_QC.coefLabel_Cortex];
figure('Position',[2 200 2000 750]);
plotQC(QC,corr_QC.fsidbase);
save_fig('QC_correlacion_pearson')
% %Correlation between stats and masks of the aseg
% figure('Position',[2 200 2000 750]);
% Tresults=innerjoin(Tresults,vol_QC)
% vol_QC([46,64,65,102,103],:)=[];
% Tresults(end,:)=[];
%  subplot(2,2,1);plot(Tresults.LHippVol,vol_QC.LHipp_vol,'+'); %vol_QC
% subplot(2,2,2);plot(Tresults.RHippVol,vol_QC.RHipp_vol,'+');
% subplot(2,2,3);plot(Tresults.LCortVol,vol_QC.LCortex_vol,'+');
% subplot(2,2,4);plot(Tresults.RCortVol,vol_QC.RCortex_vol,'+');
% save_fig('QC_correlacion_scan')

%% Checking NHV and ECT in relation to Bernal data
load('ADNI791_Hipp_and_Entorh.mat',...
    'X_Hipp','Y','group','ni');
 NHV_all=(Y(:,1)+Y(:,2))./Y(:,3)*1e3;

 ECT_all=(Y(:,4)+Y(:,5))/2;
group_all=group;

mask_MCI_Bernal=(group==2) ;

NHV=NHV_all(mask_MCI_Bernal);
ECT=ECT_all(mask_MCI_Bernal);
group=group_all(mask_MCI_Bernal);
time_BS_Bernal=X_Hipp(mask_MCI_Bernal,2);

NHV_Check =(Tresults.LHippVol+Tresults.RHippVol)./Tresults.ICV*1e3;
ECT_Check =(Tresults.L_ECT+Tresults.R_ECT)/2;
% 
%  NHV_Check =(Tresults.Left_Hippocampus+Tresults.Right_Hippocampus)*1e3; %CUIDADO MIS VOLUMENES YA ESTAN NORMALIZADOS!
%  ECT_Check =(Tresults.lh_entorhinal_thickness+Tresults.rh_entorhinal_thickness)/2;

 gr_check=double(Tresults.Convert);
 
 
 figure('Position',[2 200 2000 750]);
lme_lowessPlot([Tresults.years;time_BS_Bernal],[NHV_Check;NHV],...
    .8,[gr_check;group+1]);
title("Trayectory of normalize hippocampal volumen",'FontSize',20);
xlabel("Years",'FontSize',20)
ylabel("NHV",'FontSize',20)
legend({"Jaime","Bernal"},'FontSize',20)
xlim([0 3])
save_fig('trayectory_NHV');   
 figure('Position',[2 200 2000 750]);

lme_lowessPlot([Tresults.years;time_BS_Bernal],[ECT_Check;ECT],...
    .8,[gr_check;group+1]);
title("Trayectory of enthorial thickness",'FontSize',20);
xlabel("Years",'FontSize',20)
ylabel("ECT",'FontSize',20)
legend({"Jaime","Bernal"},'FontSize',20)
% xlim([0 3])
 save_fig('trayectory_ETC');    

%% Testing atrophy

%pMCI Bernal
time_BS_Bernal=X_Hipp(mask_MCI_Bernal,2);
ni_Bernal=get_ni(time_BS_Bernal,[]);
ni_Bernal(end)=[];
BlAge=X_Hipp(mask_MCI_Bernal,12);
atrophy_Bernal_NHV_pMCI=getAtrophy([time_BS_Bernal,BlAge],ni_Bernal,...
    NHV_all(mask_MCI_Bernal));
atrophy_Bernal_ECT_pMCI=getAtrophy([time_BS_Bernal,BlAge],ni_Bernal,...
    ECT_all(mask_MCI_Bernal));

%pMCI
mask_MCI_Check= gr_check==1;

time_BS_Check=Tresults.years(mask_MCI_Check);
ni_Check=get_ni(time_BS_Check,numel(unique(Tresults.fsidbase)));
BlAge=Tresults.Age(mask_MCI_Check)-time_BS_Check;

% atrophy_Check_NHV_pMCI=getAtrophy([time_BS_Check,BlAge],ni_Check,...
%     NHV_Check(mask_MCI_Check));
% atrophy_Check_ECT_pMCI=getAtrophy([time_BS_Check,BlAge],ni_Check,...
%     ECT_Check(mask_MCI_Check));

plot_trayectory_V2(Tresults.years,NHV_Check,ni_Check,'Jaime')
%plot_trayectory_V2(time_BS_Bernal,NHV,ni_Bernal,'Bernal')

clc;
fprintf('Baseline Bernal NHV: %.2f %.2f(pMCI)\n',...
    mean(atrophy_Bernal_NHV_pMCI{1}),std(atrophy_Bernal_NHV_pMCI{1}));
fprintf('Baseline Check  NHV: %.2f %.2f(pMCI)\n',...
    mean(atrophy_Check_NHV_pMCI{1}),std(atrophy_Check_NHV_pMCI{1}));
fprintf('Atrophy  Bernal NHV: %.2f %.2f(pMCI)\n',...
    mean(atrophy_Bernal_NHV_pMCI{2}),std(atrophy_Bernal_NHV_pMCI{2}));
fprintf('Atrophy  Check  NHV: %.2f %.2f(pMCI)\n',...
    mean(atrophy_Check_NHV_pMCI{2}),std(atrophy_Check_NHV_pMCI{2}));
fprintf('Baseline Bernal ECT: %.2f %.2f(pMCI)\n',...
    mean(atrophy_Bernal_ECT_pMCI{1}),std(atrophy_Bernal_ECT_pMCI{1}));
fprintf('Baseline Check  ECT: %.2f %.2f(pMCI)\n',...
    mean(atrophy_Check_ECT_pMCI{1}),std(atrophy_Check_ECT_pMCI{1}));
fprintf('Atrophy  Bernal ECT: %.2f %.2f(pMCI)\n',...
    mean(atrophy_Bernal_ECT_pMCI{2}),std(atrophy_Bernal_ECT_pMCI{2}));
fprintf('Atrophy  Check  ECT: %.2f %.2f(pMCI)\n',...
    mean(atrophy_Check_ECT_pMCI{2}),std(atrophy_Check_ECT_pMCI{2}));

figure('Position',[2 200 2000 750]);
subplot(2,1,1);histogram(atrophy_Bernal_NHV_pMCI{1});hold on;
histogram(atrophy_Check_NHV_pMCI{1});hold off;
title('Baseline NHV ','FontSize',20);
xlabel("Baseline",'FontSize',20)
ylabel("Number of subjects",'FontSize',20)
legend({"Bernal","Jaime"},'FontSize',20)
subplot(2,1,2);histogram(atrophy_Bernal_NHV_pMCI{2});hold on;
histogram(atrophy_Check_NHV_pMCI{2});hold off;
title('Atrophy NHV ','FontSize',20);
xlabel("Atrophy",'FontSize',20)
ylabel("Number of subjects",'FontSize',20)
legend({"Bernal","Jaime"},'FontSize',20)
save_fig('histogram_NHV')
figure('Position',[2 200 2000 750]);
subplot(2,1,1);histogram(atrophy_Bernal_ECT_pMCI{1});hold on;
histogram(atrophy_Check_ECT_pMCI{1});hold off;
title('Baseline ECT ','FontSize',20');
xlabel("Baseline",'FontSize',20)
ylabel("Number of subjects",'FontSize',20)
legend({"Bernal","Jaime"},'FontSize',20)
subplot(2,1,2);histogram(atrophy_Bernal_ECT_pMCI{2});hold on;
histogram(atrophy_Check_ECT_pMCI{2});hold off;
title('Atrophy ECT ','FontSize',20);
xlabel("Atrophy",'FontSize',20)
ylabel("Number of subjects",'FontSize',20)
legend({"Bernal","Jaime"},'FontSize',20)
save_fig('histogram_ECT')




% % mask_err_NHV=atrophy_check_NHV{2}>2 |atrophy_check_NHV{2}<-7.5;
% % id_err_NHV= TQC.ID_Base(mask_err_NHV,:);
% % atr_err_NHV=atrophy_check_NHV{2}(mask_err_NHV);
% % for i=1:length(atr_err_NHV)
% %     fprintf('NHV: %s %.2f\n',id_err_NHV(i,:), atr_err_NHV(i));
% % end
% % 
% % mask_err_ECT=atrophy_check_ECT{2}>3 |atrophy_check_ECT{2}<-10;
% % id_err_ECT= TQC.ID_Base(mask_err_ECT,:);
% % atr_err_ECT=atrophy_check_ECT{2}(mask_err_ECT);
% % for i=1:length(atr_err_ECT)
% %     fprintf('ECT: %s %.2f\n',id_err_ECT(i,:), atr_err_ECT(i));
% % end
% % Clasification
% n_boot=50;
% stat_NHV=zeros(n_boot,6);
% stat_ECT=zeros(n_boot,6);
% stat_2F =zeros(n_boot,6);
% 
% %Bernal    
% % mask_MCI_Bernal= (group_all==2) |(group_all==3); % pMCI
% % time_BS=X_Hipp(mask_MCI_Bernal,2);
% % ni=get_ni(time_BS);
% % BlAge=X_Hipp(mask_MCI_Bernal,12);
% % gr_pMCI=double(group_all(mask_MCI_Bernal)==2);
% % %X=[intercept,time_scans,group,time_scans.*group,AgeBaselina];
% % X=[ones(size(gr_pMCI,1),1),time_BS,gr_pMCI,time_BS.*gr_pMCI,BlAge];
% % NHV=NHV_all(mask_MCI_Bernal);
% % ECT=ECT_all(mask_MCI_Bernal);
% 
% % % Check
% time_BS=Tresults.years;
% BlAge=Tresults.Age-time_BS;
% ni=get_ni(time_BS,[]);
%  gr_pMCI=double(gr_check);
% %X=[intercept,time_scans,group,time_scans.*group,AgeBaselina];
% X=[ones(size(gr_pMCI,1),1),time_BS,gr_pMCI,time_BS.*gr_pMCI,BlAge];
% NHV=NHV_Check;
% ECT=ECT_Check;
% 
% parfor i=1:n_boot
%     clc;
%     fprintf('Sample %d\n',i);
%     [mask_scan_train,ni_two_train,ni_two_test]=splitSamples(ni,.75);    
%     
%     %% Univariate
%     model_LME = getLME_Model(X(mask_scan_train,:),NHV(mask_scan_train),ni_two_train);
%     stat_NHV(i,:) = testLME(model_LME,X(mask_scan_train==0,:),NHV(mask_scan_train==0),ni_two_test);
%     
%     model_LME = getLME_Model(X(mask_scan_train,:),ECT(mask_scan_train),ni_two_train);
%     stat_ECT(i,:) = testLME(model_LME,X(mask_scan_train==0,:),ECT(mask_scan_train==0),ni_two_test);
%         
%     stat_2F(i,:)=getClassLMEmulti(X,[NHV,ECT],mask_scan_train,ni_two_train,ni_two_test);
%       
% end
% printfResults(stat_NHV);
% printfResults(stat_ECT);
% printfResults(stat_2F);

% % Bernal
% % 69.1 (68.5,69.7) 67.2 (66.6,67.7) 68.0 (67.6,68.4) 0.732 (0.728,0.736)
% % 65.1 (64.5,65.7) 69.4 (68.8,69.9) 67.6 (67.2,67.9) 0.711 (0.707,0.715)
% % 70.5 (69.9,71.1) 66.6 (66.1,67.2) 68.3 (67.9,68.7) 0.747 (0.743,0.751)
% % 1617_Eskilsen
% % 69.9 (69.0,70.8) 66.7 (65.7,67.7) 68.4 (67.8,69.0) 0.740 (0.734,0.746)
% % 64.1 (63.2,65.0) 63.2 (62.2,64.2) 63.7 (63.1,64.3) 0.692 (0.685,0.699)
% % 69.2 (68.3,70.1) 61.8 (60.8,62.8) 65.8 (65.2,66.4) 0.736 (0.730,0.742)
% % 490
% % 63.5 (62.5,64.5) 61.9 (61.1,62.8) 62.7 (62.1,63.3) 0.706 (0.699,0.712)
% % 68.8 (67.9,69.7) 63.4 (62.4,64.3) 65.8 (65.1,66.4) 0.700 (0.693,0.706)
% % 70.2 (69.4,71.1) 63.6 (62.7,64.5) 66.5 (65.9,67.1) 0.717 (0.711,0.723)
% % 434
% % 70.6 (69.6,71.7) 68.8 (67.7,69.8) 69.8 (69.2,70.5) 0.757 (0.750,0.764)
% % 69.5 (68.5,70.4) 66.7 (65.6,67.8) 68.2 (67.5,68.9) 0.732 (0.725,0.740)
% % 72.6 (71.7,73.5) 66.2 (65.2,67.3) 69.7 (69.1,70.4) 0.762 (0.755,0.769)
% % 534
% % 70.1 (69.2,71.0) 67.3 (66.4,68.3) 68.8 (68.2,69.4) 0.754 (0.748,0.761)
% % 66.3 (65.4,67.3) 63.5 (62.5,64.4) 65.0 (64.4,65.6) 0.710 (0.703,0.716)
% % 70.5 (69.6,71.4) 62.6 (61.6,63.5) 66.8 (66.2,67.3) 0.755 (0.749,0.761)
% % 668
% % 66.8 (65.9,67.8) 61.7 (60.9,62.4) 64.0 (63.5,64.5) 0.699 (0.693,0.705)
% % 66.5 (65.5,67.4) 64.5 (63.7,65.2) 65.4 (64.8,65.9) 0.697 (0.691,0.703)
% % 68.2 (67.3,69.1) 62.4 (61.6,63.2) 65.1 (64.6,65.6) 0.714 (0.708,0.720)
% 

end


% function TMarkers=convertAsegAparc2Table(aseg,asegcols,aparc_lh,...
%     aparc_lhcols,aparc_rh,aparc_rhcols)
% 
% asegcols=cellstr(asegcols); % convert column names into cell string
% id=[find(strcmp('Left-Hippocampus',asegcols)==1),...
%     find(strcmp('Right-Hippocampus',asegcols)==1),...
%     find(strcmp('EstimatedTotalIntraCranialVol',asegcols)==1),...
%     find(strcmp('lhCortexVol',asegcols)==1),...
%     find(strcmp('rhCortexVol',asegcols)==1),...    
%     ];
% HV = aseg(:,id);
% TAseg=table(HV(:,1),HV(:,2),HV(:,3),HV(:,4),HV(:,5),...
%     'VariableNames',{'LHippVol','RHippVol','ICV','LCortVol','RCortVol'});
% 
% aparc_lhcols=cellstr(aparc_lhcols); % convert column names into cell string
% id_lh=[find(strcmp('lh_entorhinal_thickness',aparc_lhcols)==1),...
%     find(strcmp('lh_temporalpole_thickness',aparc_lhcols)==1),...
%     find(strcmp('lh_inferiortemporal_thickness',aparc_lhcols)==1),...
%     find(strcmp('lh_middletemporal_thickness',aparc_lhcols)==1),...
%     find(strcmp('lh_inferiorparietal_thickness',aparc_lhcols)==1),...
%     find(strcmp('lh_superiorparietal_thickness',aparc_lhcols)==1),...
%     find(strcmp('lh_precuneus_thickness',aparc_lhcols)==1),...
%     find(strcmp('lh_posteriorcingulate_thickness',aparc_lhcols)==1),...
%     find(strcmp('lh_MeanThickness_thickness',aparc_lhcols)==1)];
% 
%  aparc_rhcols=cellstr(aparc_rhcols); % convert column names into cell string           
%  id_rh=[find(strcmp('rh_entorhinal_thickness',aparc_rhcols)==1),...
%     find(strcmp('rh_temporalpole_thickness',aparc_rhcols)==1),...
%     find(strcmp('rh_inferiortemporal_thickness',aparc_rhcols)==1),...
%     find(strcmp('rh_middletemporal_thickness',aparc_rhcols)==1),...
%     find(strcmp('rh_inferiorparietal_thickness',aparc_rhcols)==1),...
%     find(strcmp('rh_superiorparietal_thickness',aparc_rhcols)==1),...
%     find(strcmp('rh_precuneus_thickness',aparc_rhcols)==1),...
%     find(strcmp('rh_posteriorcingulate_thickness',aparc_rhcols)==1),...
%     find(strcmp('rh_MeanThickness_thickness',aparc_rhcols)==1)];
% 
% TH=[aparc_lh(:,id_lh),aparc_rh(:,id_rh)];
% TApar=table(TH(:,1),TH(:,10),TH(:,9),TH(:,18),...
%     'VariableNames',{'L_ECT','R_ECT','L_MT','R_MT'});
% TMarkers=[TAseg,TApar];
% 
% end



function printfResults(stat_LME)
SEN_Su_LME = confidenceIntervals(stat_LME(:,1))*100;
SPE_Su_LME = confidenceIntervals(stat_LME(:,2))*100;
ACC_Su_LME = confidenceIntervals(stat_LME(:,5))*100;
AUC_Su_LME = confidenceIntervals(stat_LME(:,6));
% fprintf('SEN-LME: %.1f (%.1f,%.1f), SPE:  %.1f (%.1f,%.1f), ACC: %.1f (%.1f,%.1f), AUC: %.3f (%.3f,%.3f)) (Subjects-LME)\n',...
%     mean(stat_LME(:,1))*100,SEN_Su_LME(1),SEN_Su_LME(2),mean(stat_LME(:,2))*100,SPE_Su_LME(1),SPE_Su_LME(2),...
%     mean(stat_LME(:,5))*100,ACC_Su_LME(1),ACC_Su_LME(2),mean(stat_LME(:,6)),AUC_Su_LME(1),AUC_Su_LME(2));
fprintf('%.1f (%.1f,%.1f) %.1f (%.1f,%.1f) %.1f (%.1f,%.1f) %.3f (%.3f,%.3f)\n',...
    mean(stat_LME(:,1))*100,SEN_Su_LME(1),SEN_Su_LME(2),mean(stat_LME(:,2))*100,SPE_Su_LME(1),SPE_Su_LME(2),...
    mean(stat_LME(:,5))*100,ACC_Su_LME(1),ACC_Su_LME(2),mean(stat_LME(:,6)),AUC_Su_LME(1),AUC_Su_LME(2));

end


function CI = confidenceIntervals(x)
SEM = std(x)/sqrt(length(x));               % Standard Error
ts = tinv([0.025  0.975],length(x)-1);      % T-Score
CI = mean(x) + ts*SEM;                      % Confidence Intervals
end

function [mask_scan_train,ni_two_train,ni_two_test]=splitSamples(ni_two,percent)
 numPatients=length(ni_two);
 p = randperm(numPatients);
 test=p(fix(percent*numPatients)+1:end);
 flag_train=true(numPatients,1);
 for i=1:length(test)
    flag_train(test(i))=false;
 end
 
 ni_two_train=zeros(sum(flag_train==true),1);
 ni_two_test=zeros(numPatients-sum(flag_train==true),1);
 j=1;
 k=1;
 numImg=1;
 mask_scan_train=true(sum(ni_two),1);
 for i=1:numPatients
     if(flag_train(i)==true)
        ni_two_train(j)= ni_two(i);
        j=j+1;
     else
        ni_two_test(k)= ni_two(i);
        k=k+1;
        mask_scan_train(numImg:numImg+ni_two(i)-1)=false;
     end
     numImg=numImg+ni_two(i);
 end
 
  
end

function model = getLME_Model(X,Y,ni_two)
total_vol_stats = lme_fit_FS(X,[1 2],Y,ni_two);

X_gr2=X;
group_two=X(:,3);

X_gr2(:,3)=1;
X_gr2(:,4)=X(:,2);
Y_gr2= ((total_vol_stats.Bhat)'*(X_gr2)')';

err=(Y-Y_gr2);

model=cell(2,1);
model{1,1}=total_vol_stats;



%% Subjects
numPatients=length(ni_two);
diagn_two_GT=zeros(numPatients,1);
err2_mean=zeros(numPatients,1);

begin_index=0;
for i=1:numPatients
    numImgs=ni_two(i);
    begin_index=begin_index+1;
    end_index=begin_index+numImgs-1;
    err2_mean(i,:)=mean(err(begin_index:end_index,:));
    diagn_two_GT(i)=group_two(begin_index);
    begin_index=end_index;

end
t = templateDiscriminant();

model{2,1} = fitcecoc(err2_mean,diagn_two_GT,'Learners',t,'FitPosterior',1,'Prior',[.5,.5]);

end


function acc = testLME(model_LME,X,Y,ni_two_test)
X_gr2=X;
group_two_test=X(:,3);
group_two_test(group_two_test==0)=10;
group_two_test(group_two_test==1)=20;


X_gr2(:,3)=1;
X_gr2(:,4)=X(:,2);
Y_gr2= ((model_LME{1}.Bhat)'*(X_gr2)')';
err=(Y-Y_gr2);


%% Subjects
numPatients=length(ni_two_test);
diagn_two_GT_test=zeros(numPatients,1);
err2_mean=zeros(numPatients,1);

begin_index=0;
for i=1:numPatients
    numImgs=ni_two_test(i);
    begin_index=begin_index+1;
    end_index=begin_index+numImgs-1;
    err2_mean(i,:)=mean(err(begin_index:end_index,:));

    diagn_two_GT_test(i)=group_two_test(begin_index);
    begin_index=end_index;

end

score=zeros(numPatients,2);
for i=1:numPatients
    [~,~,~,score(i,:)]=predict(model_LME{2},err2_mean(i,:));
end

two_labels=unique(group_two_test);
[~,class]=max(score,[],2);
class(class==2)=two_labels(2);
class(class==1)=two_labels(1);


% Quality
[C_all,~] = confusionmat(diagn_two_GT_test,class);
TN=C_all(1,1);
FN=C_all(2,1);
FP=C_all(1,2);
TP=C_all(2,2);
sensitivy=TP/(TP+FN);
specificity=TN/(TN+FP);
PPV = TP/(TP+FP);
NPV= TN/(TN+FN);
ACC= (TN+TP)/(TN+TP+FN+FP);
[~,~,~,auc]=perfcurve(diagn_two_GT_test,score(:,2),two_labels(2));
acc=[sensitivy,specificity,PPV,NPV,ACC,auc];

end



function plotQC(QC,fsidbase)


subplot(1,2,1);
hold on;
plot(QC(:,1),QC(:,2),'r+');
axis([0 1 0 1]);
index_outliers_Hipp=find(QC(:,1)<.8 & QC(:,2)<.7);
if(isempty(index_outliers_Hipp)==0)
    for i=1:length(index_outliers_Hipp)
        text(QC(index_outliers_Hipp(i),1),QC(index_outliers_Hipp(i),2),...
            fsidbase(index_outliers_Hipp(i),:),'Interpreter','none');
    end
end
box on;

hold off;
% legend('NC','sMCI','pMCI','AD');
xlabel('Intensity correlation coefficients','FontSize',20);
ylabel('Labeling dice coefficients','FontSize',20);
title({'Lowest correlations between pairs of scans' ;'from each subject for the hippocampus'},'FontSize',20);

subplot(1,2,2);
hold on;
plot(QC(:,3),QC(:,4),'r+');
axis([0 1 0 1]);
%index_outliers_Cortex=find(QC(:,3)<.98 & QC(:,4)<.8);
index_outliers_Cortex=find(QC(:,3)<.95 & QC(:,4)<.7);

if(isempty(index_outliers_Cortex)==0)
    for i=1:length(index_outliers_Cortex)
        text(QC(index_outliers_Cortex(i),3),QC(index_outliers_Cortex(i),4),...
            fsidbase(index_outliers_Cortex(i),:),'Interpreter','none');
    end
end
box on;

hold off;
axis([0 1 0 1]);
% legend('NC','sMCI','pMCI','AD');
% legend('NC','MCI','AD');
xlabel('Intensity correlation coefficients','FontSize',20);
ylabel('Labeling dice coefficients','FontSize',20);
title({'Lowest correlations between pairs of scans ';'from each subject for the cortex'},'FontSize',20);


end


function ni = get_ni(time_visit,numIDs)
baseline=find(time_visit==0);
numScans=length(time_visit);
ni=baseline([2:end,end])-baseline;
ni(end)=numScans-baseline(end)+1;
if(isempty(numIDs)==0)
    if(length(ni)~=numIDs)
        warning('Error calculating ni: There are more scans in baseline than subjects');
    end
end
if(sum(ni)~=numScans)
   warning('Error calculating ni');
end

end


function atrophy=getAtrophy(X,ni,HippMarker)


time=X(:,1);
BlAge=X(:,2);


BlAge_ni=BlAge(time==0);

atrophy=cell(2,1);
intercept=ones(length(time),1);
X_Hipp=[intercept,time];%,BlAge];
model=lme_fit_FS(X_Hipp,[1 2],HippMarker,ni);

% atrophy{1}=model.Bhat(1)+(model.Bhat(3)*BlAge_ni')+model.bihat(1,:);
% atrophy{2}=(((model.Bhat(2)+model.bihat(2,:)))./...
%     ((model.Bhat(1)+(model.Bhat(3)*BlAge_ni')+model.bihat(1,:)))*100);

atrophy{1}=model.Bhat(1)+model.bihat(1,:);
atrophy{2}=(((model.Bhat(2)+model.bihat(2,:)))./...
    ((model.Bhat(1)+model.bihat(1,:)))*100);


end


