function corrAsegADNI_QC(Tresults)
 %Analiza la correlación entre la volumetría hipocampal y el valor de ICV de ASEG, en cada visita, con los valores dado en ADNIMERGE que emplea FSv4.4-cross.
 %Si un punto sale de la tendencia lineal habrá que inspeccionar la imagen de esa visita.


%% Hippocampal volume
aseg_HippVol_long=Tresults.LHippVol+Tresults.RHippVol;
adni_HippVol_cros=Tresults.Hippocampus;
mask_no_nan=isnan(adni_HippVol_cros)==0;
subplot(1,2,1);
plot(aseg_HippVol_long(mask_no_nan),adni_HippVol_cros(mask_no_nan),'+');
r=corrcoef(aseg_HippVol_long(mask_no_nan),adni_HippVol_cros(mask_no_nan));
title("Hippocampal volume correlation",'FontSize',10);
fprintf('Hippocampal volume correlation %.3f\n',r(1,2));
%% ICV Hippocampal volume
aseg_IVC_long=Tresults.ICV;
adni_ICV_cross=Tresults.ICV_ADNI;
mask_no_nan=isnan(adni_ICV_cross)==0;
subplot(1,2,2);
plot(aseg_IVC_long(mask_no_nan),adni_ICV_cross(mask_no_nan),'+');
r=corrcoef(aseg_IVC_long(mask_no_nan),adni_ICV_cross(mask_no_nan));
title("ICV correlation",'FontSize',10);
fprintf('ICV correlation %.3f\n',r(1,2));


end
