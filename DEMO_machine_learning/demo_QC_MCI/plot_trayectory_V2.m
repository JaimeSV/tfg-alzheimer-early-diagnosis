function []=plot_trayectory_V2(years,marker,ni,name)
begin=1;
scans=[];
 figure('Position',[2 200 2000 750]);

for i=1:size(ni,1)
    switch ni(i)
        %         case 1
        %             subplot(2,3,1)
        %             title("1 scan")
        case 2
            subplot(2,2,1)
            title("2 scan",'FontSize',20)
            ylim([min(marker),max(marker)])
        case 3
            subplot(2,2,2)
            title("3 scan",'FontSize',20)
            ylim([min(marker),max(marker)])
            %         case 4
            %             subplot(2,3,4)
            %             title("4 scan")
        case 5
            subplot(2,2,3)
            title("5 scan",'FontSize',20)
            ylim([min(marker),max(marker)])
        case 6
            subplot(2,2,4)
            ylim([min(marker),max(marker)])
            title("6 scan",'FontSize',20)
    end
    scans=[scans;ones(ni(i),1)*ni(i)];
    hold on;
    plot(years(begin:begin+ni(i)-1),marker(begin:begin+ni(i)-1))
    begin=begin+ni(i);
end
if nargin==4
    save_fig(strcat('trayectorias_separadas',name))
    end
% for i=1:size(scans,1)
% if scans(i)~=6
%    scans(i)=1; 
% end
% end
 figure('Position',[2 200 2000 750]);

lme_lowessPlot(years,marker,.8,scans)
legend('1','2','3','4','5','6')
hold off;
if nargin==4
    save_fig(strcat('lowess_plot',name))
end
end