function []=plot_trayectory(years,marker,ni)
begin=1;
figure();
for i=1:size(ni,1)
    switch ni(i)
%         case 1
%             subplot(2,3,1)
%             title("1 scan")
        case 2
            subplot(2,2,1)
            title("2 scan") 
            ylim([min(marker),max(marker)])
            scan2(1,:)=years(begin:begin+ni(i)-1);
            scan2(2,:)=marker(begin:begin+ni(i)-1);
        case 3
            subplot(2,2,2)
            title("3 scan")  
            ylim([min(marker),max(marker)])
             scan3(1,:)=years(begin:begin+ni(i)-1);
            scan3(2,:)=marker(begin:begin+ni(i)-1);
%         case 4
%             subplot(2,3,4)
%             title("4 scan")  
        case 5
            subplot(2,2,3)
            title("5 scan")
            ylim([min(marker),max(marker)])
             scan5(1,:)=years(begin:begin+ni(i)-1);
            scan5(2,:)=marker(begin:begin+ni(i)-1);
        case 6
            subplot(2,2,4)
            ylim([min(marker),max(marker)])
            title("6 scan")
            scan6(1,end+1)=years(begin:begin+ni(i)-1);
            scan6(2,:)=marker(begin:begin+ni(i)-1);
    end
    hold on;
    plot(years(begin:begin+ni(i)-1),marker(begin:begin+ni(i)-1))
%     if atrophy_Check_NHV_pMCI{2}(i)>0
%         fprintf('Subject %s has a positive atrophy \n',Tresults.fsidbase{begin});
%     end
    begin=begin+ni(i); 
end
    subplot(2,2,4);
    p=plot(mean(scan6(2,:)));
    p.LineWidth = 2;
    p.Marker = '*';
    hold off;

end
