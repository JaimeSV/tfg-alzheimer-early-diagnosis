asegAparc2table

Convierte los ficheros aseg y aparc en tablas. A�ade a cada sujeto y visita los datos demogr�ficos y cl�nicos de la tabla de ADNI (ver ADNI_clinicalDemogr.mat)

getAsegAparcADNI

Antes de ejecutar este script, establecer las variables ambientes:

%% Freesurfer variables
% export FREESURFER_HOME=/usr/local/freesurfer
% source /usr/local/freesurfer/SetUpFreeSurfer.sh
% export SUBJECTS_DIR=/home/jsimarro/Desktop/DEMO/Long


Arrancar matlab. Indicar los path de los ficheros XML y el recon-all long. Se ha resuelto el problema de las lectura de los ficheros XML y de la columna fsid-base.

En la ejecuci�n del recon-all puede ser que algunas caracter�sticas de las ROIs de aseg no se encuentren calculadas. Habr� de a�adir el flag '--common-segs':

command= strcat('asegstats2table --qdec-long',32,name_dat,32,'-t ./aseg_long.',name_dat,32,'--common-segs');


