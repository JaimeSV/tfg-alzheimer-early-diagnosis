function []=asegAparc2table
%function Tresults=asegAparc2table

%addpath(genpath('/usr/local/freesurfer/'));
addpath(genpath('../lme/'));
addpath(genpath('../fsfast/'));
if 1
    %% pMCI
    %% Data from Jaime
    addpath(('../Data_all/MCI_1718'));
    fdemographic='clinical_Jaime_1718.dat';
    faseg='aseg_long.pMCI_Jaime_1718.dat';
    faparc_lh='aparc_long_lh.pMCI_Jaime_1718.dat';
    faparc_rh='aparc_long_rh.pMCI_Jaime_1718.dat';
    % Read demographic and clinical data
    Tdec=readClinicalData(fdemographic);
    % Read aseg+aparc table
    TMarkers=readAsegAparc(faseg,faparc_lh,faparc_rh);
    Tresults1=[Tdec,TMarkers];
      %% Data from Javier
    addpath(('../Data_all/MCI_1718'));
    fdemographic='clinical_MCI_jrojo_1718.dat';
    faseg='aseg_long.clinical_MCI_jrojo_1718.dat';
    faparc_lh='aparc_long_lh.clinical_MCI_jrojo_1718.dat';
    faparc_rh='aparc_long_rh.clinical_MCI_jrojo_1718.dat';
    % Read demographic and clinical data
    Tdec=readClinicalData(fdemographic);
    % Read aseg+aparc table
    TMarkers=readAsegAparc(faseg,faparc_lh,faparc_rh);
    Tresults2=[Tdec,TMarkers];
    %% sMCI
      %% Data from Wushi
    addpath(('../Data_all/MCI_1718'));
    fdemographic='clinical_sMCI_jwu.dat';
    faseg='aseg_long.clinical_sMCI_jwu.dat';
    faparc_lh='aparc_long_lh.clinical_sMCI_jwu.dat';
    faparc_rh='aparc_long_rh.clinical_sMCI_jwu.dat';
    % Read demographic and clinical data
    Tdec=readClinicalData(fdemographic);
    % Read aseg+aparc table
    TMarkers=readAsegAparc(faseg,faparc_lh,faparc_rh);
    Tresults3=[Tdec,TMarkers];
        Tresults=[Tresults1;Tresults2;Tresults3];

end
    

if 0
    %% MCI
    %% Data from Lin
    addpath(('../Data_all/MCI_1617'));
    fdemographic='clinical_MCI490_1617.dat';
    faseg='aseg_long.clinical_MCI490_1617.dat';
    faparc_lh='aparc_long_lh.clinical_MCI490_1617.dat';
    faparc_rh='aparc_long_rh.clinical_MCI490_1617.dat';
    % Read demographic and clinical data
    Tdec=readClinicalData(fdemographic);
    % Read aseg+aparc table
    TMarkers=readAsegAparc(faseg,faparc_lh,faparc_rh);
    Tresults1=[Tdec,TMarkers];
    %% Data from Sergio
    fdemographic='clinical_MCI_116_1617.dat';
    faseg='aseg_long.clinical_MCI_116_1617.dat';
    faparc_lh='aparc_long_lh.clinical_MCI_116_1617.dat';
    faparc_rh='aparc_long_rh.clinical_MCI_116_1617.dat';
    % Read demographic and clinical data
    Tdec=readClinicalData(fdemographic);
    % Read aseg+aparc table
    TMarkers=readAsegAparc(faseg,faparc_lh,faparc_rh);
    Tresults2=[Tdec,TMarkers];
    %% Meger both
    TresultsB=[Tresults1;Tresults2];
end
if 0
    %% NC vs AD
    addpath(('../Data_all/NC_AD'));
    % 107
    fdemographic='107_NC_AD.dat';
    faseg='aseg.107_NC_AD.table';
    faparc_lh='aparc.107_NC_AD.table';
    faparc_rh='aparc.107_NC_AD.rh.table';
    % Read demographic and clinical data
    Tdec=readClinicalData(fdemographic);
    % Read aseg+aparc table
    TMarkers=readAsegAparc(faseg,faparc_lh,faparc_rh);
    Tresults1=[Tdec,TMarkers];
    % 174
    fdemographic='174_NC_AD.dat';
    faseg='aseg.174_NC_AD.table';
    faparc_lh='aparc.174_NC_AD.table';
    faparc_rh='aparc.174_NC_AD.rh.table';
    % Read demographic and clinical data
    Tdec=readClinicalData(fdemographic);
    % Read aseg+aparc table
    TMarkers=readAsegAparc(faseg,faparc_lh,faparc_rh);
    Tresults2=[Tdec,TMarkers];
    % 1103
    fdemographic='1103_NC_AD.dat';
    faseg='aseg.1103_NC_AD.table';
    faparc_lh='aparc.1103_NC_AD.table';
    faparc_rh='aparc.1103_NC_AD.rh.table';
    % Read demographic and clinical data
    Tdec=readClinicalData(fdemographic);
    % Read aseg+aparc table
    TMarkers=readAsegAparc(faseg,faparc_lh,faparc_rh);
    Tresults3=[Tdec,TMarkers];
    % Meger both
    Tresults=[Tresults1;Tresults2;Tresults3];
end
%% My data
% fdemographic='clinical_pMCI_145_1718.dat';
% faseg='aseg_long.clinical_pMCI_145_1718.dat';
% faparc_lh='aparc_long_lh.clinical_pMCI_145_1718.dat';
% faparc_rh='aparc_long_rh.clinical_pMCI_145_1718.dat';
% %% Read demographic and clinical data
% Tdec=readClinicalData(fdemographic);
% %% Read aseg+aparc table
% TMarkers=readAsegAparc(faseg,faparc_lh,faparc_rh);
% Tresults=[Tdec,TMarkers];


%% Joint ADNI table
load('ADNI_clinicalDemogr','ADNIMERGE2_v1');

%[MCI_m36,~]=selectedMCI_Patients_m36(ADNIMERGE2_v1);
%[~,Tresults]=getIndex_ADNI_MCI(MCI_m36,Tresults); %MCI_m36 ADNI table, Tresults our table with aseg,aparch and clinical data
% save('../Data/6680_Jaime','Tresults');
[MCI_m36,~]=selectedMCI_Patients_m36(ADNIMERGE2_v1); 
%     Tresults=[TresultsA;TresultsB];

[~,Tresults]=getIndex_ADNI_MCI(MCI_m36,Tresults); %MCI_m36 ADNI table, Tresults our table with aseg,aparch and clinical data
save('../Data_all/17_18','Tresults');

rmpath('../lme/');
rmpath('../fsfast/');
end


function Tdec=readClinicalData(fname)
Qdec = fReadQdec(fname);
Tdec=cell2table(Qdec(2:end,:));
varname=cellstr(Qdec(1,:));
varname{2}='fsidbase';
Tdec.Properties.VariableNames=varname;
Tdec.years=str2double(Tdec.years);
Tdec.Age=str2double(Tdec.Age);
Tdec.MMSE=str2double(Tdec.MMSE);
Tdec.GDS=str2double(Tdec.GDS);
Tdec.CDR=str2double(Tdec.CDR);
Tdec.APOE_A1=str2double(Tdec.APOE_A1);
Tdec.APOE_A2=str2double(Tdec.APOE_A2);
end

%% Convert Aseg and Aparch files into a table, its possible choose markes as we want.

function [TMarkers]=readAsegAparc(fname_aseg,fname_aparc_lh,fname_aparc_rh)
%% SUBCORTEX: Aseg
key_features_aseg={'Left-Hippocampus','Right-Hippocampus','lhCortexVol',...
    'rhCortexVol','Left-Putamen','Right-Putamen','Left-Pallidum','Right-Pallidum','Left-Caudate','Right-Caudate',...
    'Left-Amygdala','Right-Amygdala'};

[aseg,~,asegcols] =  fast_ldtable(fname_aseg);
[aseg_features,name_aseg_features]=add_key_features(aseg,asegcols,key_features_aseg);
[ICV,~]=add_key_features(aseg,asegcols,{'EstimatedTotalIntraCranialVol'});
aseg=aseg_features./ICV; %% NORMALIZATION !!!!!!!!
fprintf('I will perform the normalization in the subcortex using EstimatedTotalIntraCranialVol \n')
name_aseg_features = regexprep(name_aseg_features, '-', '_');
TAseg=table(aseg(:,1),aseg(:,2),aseg(:,3),aseg(:,4),aseg(:,5),aseg(:,6),aseg(:,7),aseg(:,8),aseg(:,9),aseg(:,10),...
    aseg(:,11),aseg(:,12),'VariableNames',name_aseg_features);
%% CORTEX: APARC
% LH
key_features_aparc_lh={'lh_inferiortemporal_thickness','lh_entorhinal_thickness','lh_temporalpole_thickness',...
    'lh_middletemporal_thickness','lh_inferiorparietal_thickness',...
    'lh_posteriorcingulate_thickness','lh_caudalanteriorcingulate_thickness','lh_cuneus_thickness',...
    'lh_fusiform_thickness','lh_isthmuscingulate_thickness','lh_parahippocampal_thickness','lh_precuneus_thickness',...
    'lh_superiorparietal_thickness','lh_superiortemporal_thickness','lh_transversetemporal_thickness','lh_insula_thickness'};

[aparc_lh,~,aparc_lh_cols] =  fast_ldtable(fname_aparc_lh);
[aparc_lh_features,name_aparc_lh_features]=add_key_features(aparc_lh,aparc_lh_cols,key_features_aparc_lh);
TAparc=table();

for i=1:size(aparc_lh_features,2)
    TAparc=[TAparc,table(aparc_lh_features(:,i),'VariableNames',name_aparc_lh_features(i))];
end

% RH
key_features_aparc_rh={'rh_inferiortemporal_thickness','rh_entorhinal_thickness','rh_temporalpole_thickness',...
    'rh_middletemporal_thickness','rh_inferiorparietal_thickness',...
    'rh_precuneus_thickness','rh_posteriorcingulate_thickness','rh_caudalanteriorcingulate_thickness','rh_cuneus_thickness',...
    'rh_fusiform_thickness','rh_isthmuscingulate_thickness','rh_parahippocampal_thickness',...
    'rh_superiorparietal_thickness','rh_superiortemporal_thickness','rh_transversetemporal_thickness','rh_insula_thickness'};

[aparc_rh,~,aparc_rh_cols] =  fast_ldtable(fname_aparc_rh);
[aparc_rh_features,name_aparc_rh_features]=add_key_features(aparc_rh,aparc_rh_cols,key_features_aparc_rh);

for i=1:size(aparc_rh_features,2)
    TAparc=[TAparc,table(aparc_rh_features(:,i),'VariableNames',name_aparc_rh_features(i))];
end
TMarkers=[TAseg,TAparc];


end

function[features,name_features]=add_key_features(data,name_data,key_features)
features=[];
name_features=[];
for i=1:size(key_features,2)
    for index=1:size(name_data)
        aux=name_data(index,:);
        if (strcmp(aux(1:size(key_features{i},2)),key_features{i}))
            % index=find(strcmp(key_features{i},name_data));
            features=[features,data(:,index)];
            name_features{i}=name_data(index,:);
            %name_features{i}=key_features{index};
        end
    end
end
end




function [index_ADNI,Tresults]=getIndex_ADNI_AD(MCI_m36,Tresults)
id=string(MCI_m36.PTID);
viscode=string(MCI_m36.VISCODE);
numScans=size(Tresults,1);
index_ADNI=zeros(numScans,1);
subjets=cellstr(Tresults.fsidbase);
visit_1617=string(discretize(Tresults.years,[0 .3 .95 1.45 1.95 2.45 2.95 3.45],'categorical',...
    {'bl','m06','m12','m18','m24','m30','m36'}));
delete_scans=[];
for i=1:numScans
    index_tmp=find(strcmp(id,subjets(i)) & strcmp(viscode,visit_1617(i)));
    if(isscalar(index_tmp))
        index_ADNI(i)=index_tmp;
    else
        fprintf('Wrong:  %d %s %s\n',i,string(cellstr(subjets(i))),visit_1617(i));
        delete_scans=[delete_scans;i];
    end
end
index_ADNI(delete_scans)=[];
visit_1617(delete_scans)=[];
Tresults(delete_scans,:)=[];
Tadni=MCI_m36(index_ADNI,[2,3,11,15,23:30,34,54:60]); %ID,VISCODE,EDUATION,APOE4,CLINICAL DATA,FAQ,ANATOMICAL VOLUMENS
Tadni.Properties.VariableNames{'ICV'}='ICV_ADNI';
Tadni.Properties.VariableNames{'MMSE'}='MMSE_ADNI';
Tadni.ADAS11=str2double(Tadni.ADAS11);
Tadni.ADAS13=str2double(Tadni.ADAS13);
Tadni.RAVLT_perc_forgetting=str2double(Tadni.RAVLT_perc_forgetting);
Tresults=[Tresults,table(visit_1617,'VariableNames',{'Visit'}),Tadni];
numScans=size(Tresults,1);
test_ID=false(numScans,1);
test_Visit=false(numScans,1);
for i=1:numScans
    test_Visit(i)=strcmp(Tresults.Visit(i),string(Tresults.VISCODE(i)));
    test_ID(i)=strcmp(Tresults.fsidbase(i),string(Tresults.PTID(i)));
end

if( ((sum(test_Visit)==numScans) || (sum(test_ID)==numScans)) == 0 )
    error('Error merge the tables\n');
end
end






function [index_ADNI,Tresults]=getIndex_ADNI_MCI(MCI_m36,Tresults)
id=string(MCI_m36.PTID);
viscode=string(MCI_m36.VISCODE);

numScans=size(Tresults,1);
index_ADNI=zeros(numScans,1);
subjets=cellstr(Tresults.fsidbase);
visit_1617=string(discretize(Tresults.years,[0 .3 .95 1.45 1.95 2.45 2.95 3.45],'categorical',...
    {'bl','m06','m12','m18','m24','m30','m36'}));

delete_scans=[];
for i=1:numScans
    index_tmp=find(strcmp(id,subjets(i)) & strcmp(viscode,visit_1617(i)));
    if(isscalar(index_tmp))
        index_ADNI(i)=index_tmp;
    else
        fprintf('Wrong:  %d %s %s\n',i,string(cellstr(subjets(i))),visit_1617(i));
        delete_scans=[delete_scans;i];
    end
    
end
index_ADNI(delete_scans)=[];
visit_1617(delete_scans)=[];
Tresults(delete_scans,:)=[];


Tadni=MCI_m36(index_ADNI,[2,3,11,15,23:30,34,54:60,114,115]); %ID,VISCODE,EDUATION,APOE4,CLINICAL DATA,FAQ,ANATOMICAL VOLUMENS,CONVERT,CONVERTTIME
Tadni.Properties.VariableNames{'ICV'}='ICV_ADNI';
Tadni.Properties.VariableNames{'MMSE'}='MMSE_ADNI';
Tadni.ADAS11=str2double(Tadni.ADAS11);
Tadni.ADAS13=str2double(Tadni.ADAS13);
Tadni.RAVLT_perc_forgetting=str2double(Tadni.RAVLT_perc_forgetting);

Tresults=[Tresults,table(visit_1617,'VariableNames',{'Visit'}),Tadni];

numScans=size(Tresults,1);
test_ID=false(numScans,1);
test_Visit=false(numScans,1);
for i=1:numScans
    test_Visit(i)=strcmp(Tresults.Visit(i),string(Tresults.VISCODE(i)));
    test_ID(i)=strcmp(Tresults.fsidbase(i),string(Tresults.PTID(i)));
end

if( ((sum(test_Visit)==numScans) || (sum(test_ID)==numScans)) == 0 )
    error('Error merge the tables\n');
end

end

function [MCI_m36,subjects]=selectedMCI_Patients_m36(ADNIMERGE2)

visit=ADNIMERGE2.VISCODE;
code_visit=unique(visit);
diagn_bl=ADNIMERGE2.DX_bl;
group_bl=unique(diagn_bl);
diagn_X=ADNIMERGE2.DX;
group=unique(diagn_X);

mask_MCI = diagn_bl==group_bl(3) | diagn_bl==group_bl(4) | diagn_bl==group_bl(5); %1 just EMCI LMCI and MCI
mask_follow_m36= visit==code_visit(1) | visit==code_visit(2) | visit==code_visit(3) |... % 1 just 0,3,6,12,18,24,30,36 months
    visit==code_visit(7) | visit==code_visit(12) | visit==code_visit(13) |...
    visit==code_visit(14) | visit==code_visit(15);
mask_diagn = diagn_X==group(1) | diagn_X==group(2) | diagn_X==group(3); % All

MCI_m36=ADNIMERGE2( mask_MCI & mask_follow_m36 & mask_diagn,:);
% MCI_m36=ADNIMERGE2( mask_MCI & mask_follow_m36,:);

subjects=unique(MCI_m36.PTID);
ID=MCI_m36.PTID;

convert=false(size(MCI_m36,1),1);
time_conver_cens=zeros(size(MCI_m36,1),1);


for i=1:length(subjects)
    index=find(ID==subjects(i));
    time_patient=convert_time(MCI_m36.VISCODE(index),code_visit);
    [time_patient,index_time]=sort(time_patient);
    diagn_patient=MCI_m36.DX(index);
    %new criteria
    diagn_patient=diagn_patient(index_time);
    labels_patient=unique(diagn_patient);
    if((length(labels_patient)==1) || diagn_patient(end)~=group(2))%stable
        time_conver_cens(index)=time_patient(end);
    else
        for j=1:length(index)
            if(diagn_patient(j)==group(2)) %dementia
                convert(index)=true;
                time_conver_cens(index)=time_patient(j);
                break;
            end
        end
    end
    %old criteria
    %     stable=true;
    %     for j=1:length(index)
    %         if(diagn_patient(j)==group(2)) %dementia
    %             convert(index)=true;
    %             time_conver_cens(index)=time_patient(j);
    %             stable=false;
    %             break;
    %         end
    %     end
    %     if(stable)
    %         time_conver_cens(index)=time_patient(end);
    %     end
end

MCI_m36=[MCI_m36,table(convert,time_conver_cens,'VariableNames',{'Convert','ConvertTime'})];
end

function tableMCI=stable_convertMCI(tableMCI,code_visit)

group=unique(tableMCI.DX);
subjects=unique(tableMCI.PTID);
ID=tableMCI.PTID;

convert=false(size(tableMCI,1),1);
time_conver_cens=zeros(size(tableMCI,1),1);


for i=1:length(subjects)
    index=find(ID==subjects(i));
    time_patient=convert_time(tableMCI.VISCODE(index),code_visit);
    [time_patient,index_time]=sort(time_patient);
    diagn_patient=tableMCI.DX(index);
    diagn_patient=diagn_patient(index_time);
    stable=true;
    for j=1:length(index)
        if(diagn_patient(j)==group(2)) %dementia
            convert(index)=true;
            time_conver_cens(index)=time_patient(j);
            stable=false;
            break;
        end
    end
    if(stable)
        time_conver_cens(index)=time_patient(end);
    end
end

tableMCI=[table(convert,time_conver_cens,'VariableNames',{'Convert','TimeConCen'}),tableMCI];

end


function time_patient=convert_time(visits,code)
time_patient=zeros(length(visits),1);
for i=1:length(visits)
    time_patient(i)=visit2double(visits(i),code);
end

end

function time = visit2double(visit,code)
switch(visit)
    case code(1)
        time=0;
    case code(2)
        time=3;
    case code(3)
        time=6;
    case code(7)
        time=12;
    case code(12)
        time=18;
    case code(13)
        time=24;
    case code(14)
        time=30;
    case code(15)
        time=36;
    otherwise
        time=-1;
end
end




