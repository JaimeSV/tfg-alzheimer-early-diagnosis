function [stat_feat,uni_selectedFeat]=LME_long_error(features,selectedFeat,X,...
mask_scan_train,mask_scan_test,ni_two_train,ni_two_test,n_visit)
% selectedFeat is the matrix with the indexes of the features chosen.
% n_boot is the number of iterations used in order to calculate statistical
% error.
% nitest is a parameter to choose the number of images chosen for the test.
% if nitest=-1, default behaviour is to take all visits;
% if nitest=1, takes only baseline. Other positives take more visits if
% posible.
gr_feat = selectedFeat;%%%%%%%%%%%%%%%%%%
uni_selectedFeat = unique(selectedFeat)';
if uni_selectedFeat(1)==0
    uni_selectedFeat(1)=[];
end
features=features(:,uni_selectedFeat);

gr_feat=convertFeatures(gr_feat,uni_selectedFeat);

% Normalized features
numSamples=size(features,1);
features=(features-repmat(mean(features(mask_scan_train,:)),numSamples,1))./repmat(std(features(mask_scan_train,:)),numSamples,1);
num_groups=size(gr_feat,1);

%% Clasification
 %   clc;
    %% Longitudinal residue
    [error_train,error_test,diagn_two_GT_train,diagn_two_GT_test]=...
        getLMEmulti(X,features,mask_scan_train,mask_scan_test,ni_two_train,ni_two_test,n_visit);
    
  stat_feat=zeros(num_groups,6,size(error_test,3));
    %% Classification
    for j=1:num_groups
        featSorted=unique(gr_feat(j,:));
        if(featSorted(1)==0)
            featSorted=featSorted(2:end);
        end
        if n_visit==-1
        stat_feat(j,:)=getClassifierLMEmulti(error_train(:,featSorted),...
            error_test(:,featSorted),diagn_two_GT_train,diagn_two_GT_test);
        else
            for v=1:n_visit
                stat_feat(j,:,v)=getClassifierLMEmulti(error_train(:,featSorted),...
            error_test(:,featSorted,v),diagn_two_GT_train,diagn_two_GT_test);
            end
        end
    end       
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Auxiliar

function gr_new=convertFeatures(gr_feat,selectedFeat)
gr_new=zeros(size(gr_feat));
for i=1:length(selectedFeat)
    gr_new(gr_feat(:)==selectedFeat(i))=i;
end
end

% function ni = get_ni(time_visit)
% mask_baseline=time_visit==0;
% numScans=length(time_visit);
% ni=zeros(sum(mask_baseline),1);
% j=1;
% for i=1:length(ni)
%     if(mask_baseline(j)==1 && j<numScans )
%         j=j+1;
%         k=1;
%         while(time_visit(j)>0 && j<numScans)
%             j=j+1;
%             k=k+1;
%         end
%         ni(i)=k;
%         if(j==numScans)
%             ni(i)=k+1;
%         end
%     elseif(j==numScans)
%         ni(i)=1;
%     else
%         warning('Error calculating ni');
%     end
%     
% end
% 
% end


% function printfResults(stat_LME,marker)
% SEN_Su_LME = confidenceIntervals(stat_LME(:,1))*100;
% SPE_Su_LME = confidenceIntervals(stat_LME(:,2))*100;
% ACC_Su_LME = confidenceIntervals(stat_LME(:,5))*100;
% AUC_Su_LME = confidenceIntervals(stat_LME(:,6));
% fprintf('%s: %.1f (%.1f,%.1f) %.1f (%.1f,%.1f) %.1f (%.1f,%.1f) %.3f (%.3f,%.3f)\n',marker,...
%     mean(stat_LME(:,1))*100,SEN_Su_LME(1),SEN_Su_LME(2),mean(stat_LME(:,2))*100,SPE_Su_LME(1),SPE_Su_LME(2),...
%     mean(stat_LME(:,5))*100,ACC_Su_LME(1),ACC_Su_LME(2),mean(stat_LME(:,6)),AUC_Su_LME(1),AUC_Su_LME(2));
% 
% end
% 
% 
% function CI = confidenceIntervals(x)
% SEM = std(x)/sqrt(length(x));               % Standard Error
% ts = tinv([0.025  0.975],length(x)-1);      % T-Score
% CI = mean(x) + ts*SEM;                      % Confidence Intervals
% end
% 


