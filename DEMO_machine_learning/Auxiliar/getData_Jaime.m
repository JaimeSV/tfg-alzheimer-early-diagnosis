
function  [Tresults,features,features_names,ni] = getData_Jaime(groups,cl_MCI,cl_AD)
%% Data
if(strcmp(groups,'MCI'))
    load('../Data/668_Jaime_with_cluster_from_both','Tresults');
elseif strcmp(groups,'NC_AD')
    load('../Data/NC_AD_with_cluster_from_both','Tresults');
    elseif strcmp(groups,'all_MCI')
    load('../Data/all_sMCI_pMCI_with_clusters','Tresults');
    elseif strcmp(groups,'new_MCI')
    load('../Data/17_18_with_clusters','Tresults');
        elseif strcmp(groups,'my_MCI')
    load('../Data/Jaime_V1','Tresults');
else
    error('Error, select between NC_AD or MCI groups')
end

%% MRI MARKERS
%% CORTEX
long_names={'Left_Hippocampus','Right_Hippocampus','lhCortexVol','rhCortexVol','Left_Putamen',...
    'Right_Putamen','Left_Pallidum','Right_Pallidum','Left_Caudate','Right_Caudate',...
    'Left_Amygdala','Right_Amygdala'};
short_names={'NHV_LH','NHV_RH','NCV_LH','NCV_RH','NPutV_LH',...
    'NPutV_RH','NPalV_LH','NPalV_RH','NCauV_LH','NCauV_RH',...
    'NAV_LH','NAV_RH'};

if size(long_names,2)~=size(short_names,2)
    error('Error!!! Dont match the number of short names and long names in SUBCORTEX');
end
[features_subcortex,features_names_subcortex]=add_key_features(Tresults,long_names,short_names);
%% SUBCORTEX
%LH
long_names={'lh_inferiortemporal_thickness','lh_entorhinal_thickness','lh_temporalpole_thickness','lh_middletemporal_thickness',...
    'lh_inferiorparietal_thickness','lh_posteriorcingulate_thickness','lh_caudalanteriorcingulate_thickness','lh_cuneus_thickness',...
    'lh_fusiform_thickness','lh_isthmuscingulate_thickness','lh_parahippocampal_thickness','lh_precuneus_thickness',...
    'lh_superiorparietal_thickness','lh_superiortemporal_thickness','lh_transversetemporal_thickness','lh_insula_thickness'};
short_names={'ITT_LH','ECT_LH','TempPolT_LH','MTT_LH',...
    'IPT_LH','PCingT_LH','CCingT_LH','CunT_LH',...
    'FusT_LH','IstCingT_LH','PHT_LH','PrecT_LH',...
    'SPT_LH','STT_LH','TTT_LH','InsuT_LH'};
if size(long_names,2)~=size(short_names,2)
    error('Error!!! Dont match the number of short names and long names in LH_CORTEX');
end
[features_lh_cortex,features_names_lh_cortex]=add_key_features(Tresults,long_names,short_names);

%RH
long_names={'rh_inferiortemporal_thickness','rh_entorhinal_thickness','rh_temporalpole_thickness','rh_middletemporal_thickness',...
    'rh_inferiorparietal_thickness','rh_posteriorcingulate_thickness','rh_caudalanteriorcingulate_thickness','rh_cuneus_thickness',...
    'rh_fusiform_thickness','rh_isthmuscingulate_thickness','rh_parahippocampal_thickness','rh_precuneus_thickness',...
    'rh_superiorparietal_thickness','rh_superiortemporal_thickness','rh_transversetemporal_thickness','rh_insula_thickness'};
short_names={'ITT_RH','ECT_RH','TempPolT_RH','MTT_RH',...
    'IPT_RH','PCingT_RH','CCingT_RH','CunT_RH',...
    'FusT_RH','IstCingT_RH','PHT_RH','PrecT_RH',...
    'SPT_RH','STT_RH','TTT_RH','InsuT_RH'};
if size(long_names,2)~=size(short_names,2)
    error('Error!!! Dont match the number of short names and long names in RH_CORTEX');
end
[features_rh_cortex,features_names_rh_cortex]=add_key_features(Tresults,long_names,short_names);

features=[features_subcortex,features_lh_cortex,features_rh_cortex];
features_names=[features_names_subcortex,features_names_lh_cortex,features_names_rh_cortex];
features_names=unique(features_names);
%% CAM MARKERS
CAM=[Tresults.RAVLT_forgetting,Tresults.RAVLT_immediate,...
    Tresults.RAVLT_learning,Tresults.RAVLT_perc_forgetting,...
    Tresults.ADAS11,Tresults.ADAS13,Tresults.FAQ,...
    Tresults.MMSE,Tresults.GDS,Tresults.CDR,...
    Tresults.ADASQ4];
CAM_names={'RAVLT_forgetting','RAVLT_immediate',...
    'RAVLT_learning','RAVLT_perc_forgetting',...
    'ADAS11','ADAS13','FAQ','MMSE','GDS','CDR','ADASQ4'};
time_visit=Tresults.years;
ni = get_ni(time_visit,numel(unique(Tresults.fsidbase)));
[CAM,delete]=clean_invalid_CAM(CAM,ni,time_visit);
features=[features,CAM];
[features(:,7),Tresults.FAQ]

features(delete,:)=[];
Tresults(delete,:)=[];
ni = get_ni(Tresults.years,numel(unique(Tresults.fsidbase))); %New ni with deleted scans
features_names=[features_names,CAM_names];
%% Massive
if cl_MCI
    index_clusters_MCI=find((strncmpi(Tresults.Properties.VariableNames,'lh_MCI',6) | strncmpi(Tresults.Properties.VariableNames,'rh_MCI',6)));
    long_names_MCI={};
    for i=1:size(index_clusters_MCI,2)
        long_names_MCI=[long_names_MCI,Tresults.Properties.VariableNames{index_clusters_MCI(i)}];
    end
    [features_massive_MCI,features_names_massive_MCI]=add_key_features(Tresults,long_names_MCI,long_names_MCI);
    features=[features,features_massive_MCI];
    features_names=[features_names,features_names_massive_MCI];
end
if cl_AD
    index_clusters_AD=find((strncmpi(Tresults.Properties.VariableNames,'lh_AD',5) | strncmpi(Tresults.Properties.VariableNames,'rh_AD',5)));
    long_names_AD={};
    for i=1:size(index_clusters_AD,2)
        long_names_AD=[long_names_AD,Tresults.Properties.VariableNames{index_clusters_AD(i)}];
    end
    [features_massive_AD,features_names_massive_AD]=add_key_features(Tresults,long_names_AD,long_names_AD);
    features=[features,features_massive_AD];
    features_names=[features_names,features_names_massive_AD];
end



%% CHECKING
if size(features,2)~=size(features_names,2)
    error('Error!!! Dont match the number of features and their names');
end

if find(isnan(features))
    error('Error!!! There are any NaN values in features');
end
if (sum(ni)~=size(Tresults,1) || sum(ni)~=size(features,1))
    error('Error!!! The rows dont match');
end

% %% Extact info data
% male=0;
% age=[];
% mmse=[];
% for i=1:size(Tresults,1)
%     if Tresults.years(i)==0
%         if ~strcmp(Tresults.diagnose(i),'AD')
%             if strcmp(Tresults.sex(i),'M')
%                 male=male+1;
%             end
%             age=[age;Tresults.Age(i)];
%             mmse=[mmse;Tresults.MMSE_ADNI(i)];
%         end
%     end
% end
% total=size(age,1);
% fprintf('Number %f  \n',total);
% fprintf(' Age= %f +- %f \n',mean(age) ,std(age));
% fprintf('Male percent %f  \n',(male/total))
% fprintf('MMSE%f +- %f \n',mean(mmse),std(mmse));

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
%% AUXILIAR FUNCTIONS
function[features,name_features]=add_key_features(Tresults,long_names,short_names)
features=[];
name_features={};
name_data=Tresults.Properties.VariableNames;
for i=1:size(long_names,2)
    for index=1:size(name_data,2)
        aux=name_data{index};
        if (strcmp(name_data{index},long_names{i}))
            features=[features,table2array(Tresults(:,index))];
            name_features(i)=short_names(i);
        end
    end
end
end

function ni = get_ni(time_visit,numIDs)
baseline=find(time_visit==0);
numScans=length(time_visit);
ni=baseline([2:end,end])-baseline;
ni(end)=numScans-baseline(end)+1;
if(isempty(numIDs)==0)
    if(length(ni)~=numIDs)
        warning('Error calculating ni: There are more scans in baseline than subjects');
    end
end
if(sum(ni)~=numScans)
    warning('Error calculating ni');
end

end

function [new_CAM,delete] = clean_invalid_CAM(CAM,ni,time_visit)
new_CAM = CAM;
numPatients=length(ni);
numImg=1;
fprintf('\n');
delete=[];
for i=1:numPatients    
    % if there is any element which is nan:
    tmp_CAM = CAM(numImg:numImg+ni(i)-1,:);
    invalid_visits=sum(isnan(tmp_CAM),2);
    if sum(invalid_visits)>0
        % Inside this if statement the affected visit must be modified
        % or deleted in order to avoid Nan values. If deleted, function
        % input must be changed in order to include all the features.         
        fprintf('subject %d had some nan values.\n',i);
        if (size(tmp_CAM,1)-sum(invalid_visits~=0))<2
            delete=[delete,numImg:numImg+ni(i)-1];
        else             
            for f=1:size(tmp_CAM,2)
                % if the feature has a nan element, interpolation/extrapolation
                % is executed and saved into the output matrix.
                if sum(isnan(tmp_CAM(:,f)))>0                     
                    % First the nan values are separated:
                    times = time_visit(numImg:numImg+ni(i)-1);
                    valid_idx = find(~isnan(tmp_CAM(:,f)));
                    valid_visits = tmp_CAM(valid_idx,f);
                    valid_times = times(valid_idx);                     
                    new_CAM(numImg:numImg+ni(i)-1,f) = interp1(valid_times,valid_visits,times,'linear','extrap');                                          
                end
            end
        end         
    end
    numImg = numImg+ni(i);
end
end

