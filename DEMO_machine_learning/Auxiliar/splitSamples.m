function [mask_scan_train,mask_scan_test,ni_two_train,ni_two_test]=splitSamples(ni_two,percent,nitest,group)
numPatients=length(ni_two);
selection_group=[];
while isempty(find(selection_group)) | size(find(selection_group),1)==size(selection_group,1)
    p = randperm(numPatients);
    test=p(fix(percent*numPatients)+1:end);
    flag_train=true(numPatients,1);
    
    for i=1:length(test)
        flag_train(test(i))=false;
    end
    
    % ni_two_train=zeros(sum(flag_train==true),1);%%%%%%
    % ni_two_test=zeros(numPatients-sum(flag_train==true),1);%%%%
    j=1;
    k=1;
    numImg=1;
    mask_scan_train=true(sum(ni_two),1);
    mask_scan_test=false(sum(ni_two),1);
    
    for i=1:numPatients
        if(flag_train(i)==true)
            ni_two_train(j)= ni_two(i);
            j=j+1;
        else
            if nitest>0
                % options related to limit the number of images from the test
                % test will only be valid if we have actually that many number
                % of images. 1 means baseline.
                if nitest<=ni_two(i)
                    % desired case: we have enough images.
                    ni_two_test(k)= nitest;
                    k=k+1;
                    mask_scan_train(numImg:numImg+ni_two(i)-1)=false;
                    mask_scan_test(numImg:numImg+nitest-1)=true;
                    
                else
                    % undesired case: not enough images. Invalid test subject.
                    mask_scan_train(numImg:numImg+ni_two(i)-1)=false;
                    mask_scan_test(numImg:numImg+ni_two(i)-1)=false;
                end
                
                
            else
                % default behaviour taking all posibles images.
                ni_two_test(k)= ni_two(i);
                k=k+1;
                mask_scan_train(numImg:numImg+ni_two(i)-1)=false;
                mask_scan_test(numImg:numImg+ni_two(i)-1)=true;
            end
        end
        
        numImg=numImg+ni_two(i);
    end
    if size(group,2)==1
        selection_group=group(mask_scan_test);
    else
        [~,selection_group,~]=getFixedEffects(group(mask_scan_test,:));
    end
end
end