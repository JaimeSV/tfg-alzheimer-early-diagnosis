function [error_train,error_test,diagn_two_GT_train,diagn_two_GT_test,model_zi]=...
    getLMEmulti(X,Y,mask_scan_train,mask_scan_test,ni_two_train,ni_two_test,n_visit)
%X=[ones(numScans,1),time_visit,group,group.*time_visit,age];

numFeatures=size(Y,2);
%% Class
error_train=zeros(length(ni_two_train),numFeatures);
if n_visit==-1
    error_test=zeros(length(ni_two_test),numFeatures,1);
else
    error_test=zeros(length(ni_two_test),numFeatures,n_visit);
end

for i=1:numFeatures    
    [e_train,diagn_two_GT_train,model_zi] = get_LME_err_train(X(mask_scan_train,:),Y(mask_scan_train,i),ni_two_train);
    [e_test,diagn_two_GT_test] = get_LME_err_test(model_zi,X(mask_scan_test,:),Y(mask_scan_test,i),ni_two_test,n_visit);
    error_train(:,i)=e_train;
    error_test(:,i,:)=e_test;
end

end



