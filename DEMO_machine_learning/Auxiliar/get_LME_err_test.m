function [err2_mean,diagn_two_GT_test] = get_LME_err_test(model_LME,X,Y,ni_two_test,n_visit)
X_gr2=X;
group_two_test=X(:,3);
group_two_test(group_two_test==0)=10;
group_two_test(group_two_test==1)=20;


X_gr2(:,3)=1;
X_gr2(:,4)=X(:,2);
Y_gr2= ((model_LME.Bhat)'*(X_gr2)')';
err=(Y-Y_gr2);


%% Subjects
numPatients=length(ni_two_test);
diagn_two_GT_test=zeros(numPatients,1);
if n_visit==-1
    err2_mean=zeros(numPatients,1);
else
    err2_mean=zeros(numPatients,n_visit);
end
begin_index=0;
for i=1:numPatients
    numImgs=ni_two_test(i);
    begin_index=begin_index+1;
    end_index=begin_index+numImgs-1;
    if n_visit==-1 % Just long
        err2_mean(i,:)=mean(err(begin_index:end_index,:));
    else
        for j=1:n_visit %% Calculate longitudinal residue from baseline to n_visit 
            if j>numImgs
                err2_mean(i,j)=mean(err(begin_index:end_index,:));
            else
                err2_mean(i,j)=mean(err(begin_index:begin_index+j-1,:));
            end
        end
    end
    diagn_two_GT_test(i)=group_two_test(begin_index);
    begin_index=end_index;
    
end
end