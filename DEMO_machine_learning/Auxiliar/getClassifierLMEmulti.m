function stat_class=getClassifierLMEmulti(error_train,error_test,diagn_two_GT_train,...
    diagn_two_GT_test)
t = templateDiscriminant();
model_train = fitcecoc(error_train,diagn_two_GT_train,'Learners',t,'FitPosterior',1,'Prior',[.5,.5]);
stat_class = classLME(model_train,error_test,diagn_two_GT_test);
end


function acc = classLME(model_class,err_test,diagn_two_GT_test)

numPatients=size(err_test,1);
score=zeros(numPatients,2);
for i=1:numPatients
    [~,~,~,score(i,:)]=predict(model_class,err_test(i,:));
end

two_labels=unique(diagn_two_GT_test);
[~,class]=max(score,[],2);
class(class==2)=two_labels(2);
class(class==1)=two_labels(1);


% Quality
[C_all,~] = confusionmat(diagn_two_GT_test,class);
TN=C_all(1,1);
FN=C_all(2,1);
FP=C_all(1,2);
TP=C_all(2,2);
sensitivy=TP/(TP+FN);
specificity=TN/(TN+FP);
PPV = TP/(TP+FP);
NPV= TN/(TN+FN);
ACC= (TN+TP)/(TN+TP+FN+FP);
[~,~,~,auc]=perfcurve(diagn_two_GT_test,score(:,2),two_labels(2));
acc=[sensitivy,specificity,PPV,NPV,ACC,auc];

end


