function table_sort=featureSelection(features,group,ni,dim_feat,row_max)
% This function is charged of selecting the best characteristics and
% returns the frequency of each feature. This script must be executed
% before groupFeatureSelection because here the input for that is being
% decided.
addpath(genpath('../mRMR/'));


%% Selection
numFeat=size(features,2);

%n_boot depending on dim_feat to ensure a certain frequency
n_boot=150+dim_feat*10;
freq_feat=zeros(n_boot,numFeat);
parfor i=1:n_boot
    
% for i=1:n_boot
    %clc;
    %fprintf('Selection sample %d\n',i);
    [mask_scan_train,~,ni_two_train,~]=splitSamples(ni,.75,-1,group);

    %% Feature selection
    featSorted = featureSelection_mRMR(features(mask_scan_train,:),...
        group(mask_scan_train),ni_two_train);
    freq_feat(i,:)=featSorted;
    
         
end

% clc;
%fprintf('GROUPS OF: %i\n',dim_feat);
table=freq_feat(:,1:dim_feat);
sort_frecuency(table,5);
%fprintf('The most common combination without be ordered\n')
table_sort=sort(table,2);
table_sort=sort_frecuency(table_sort,5);

% Limited by maximum row
if size(table_sort,2)>row_max
table_sort=table_sort(1:row_max,:);
end
rmpath('../mRMR/')
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Auxiliar
function featSorted = featureSelection_mRMR(features,group,ni_train)
group=double(group);
numFeat=size(features,2);
numSamples=size(features,1);

%Normalization
features=(features-repmat(mean(features),numSamples,1))./repmat(std(features),numSamples,1);

%mRMR
featSorted=mrmr_miq_d(features, group, numFeat); 

end



function [selection]= sort_frecuency(table,p_max)
num_select=0;
selection=[];
for f=1:size(table,1)
    group_feature= table(f,:);
    done=0;
    for s=1:size(selection) % Checks if group of feature has been previously done
        if selection(s,:)== group_feature
            done=1;
        end
    end
    
    if done==0 %New group of features
        num_select=num_select+1;
        selection(num_select,:)=group_feature;
        frec_select(num_select)=1;
        for t=f+1:size(table,1)
            if group_feature==table(t,:)
                frec_select(num_select)=frec_select(num_select)+1;
            end
        end
    end
end
[frec_select,index]=sort(frec_select,'descend');
selection=selection(index,:);

m = min(p_max,length(frec_select));
selection = selection(1:m,:);
end

