function[X,group,message_FixedEffects]=getFixedEffects(Tresults)

numScans=size(Tresults,1);
time_visit=Tresults.years;
age=Tresults.Age-time_visit;
if (strcmp(Tresults.diagnose(1),'MCI') || strcmp(Tresults.diagnose(1),'LMCI'))
    group=Tresults.Convert>0;
else
    group=strcmp(Tresults.diagnose,'AD');
end

e4=Tresults.APOE4;
gender=double(strcmp('F',Tresults.sex));
education=Tresults.PTEDUCAT;

X=[ones(numScans,1),time_visit,group,group.*time_visit,age];
% X=[ones(numScans,1),time_visit,group,group.*time_visit,e4,e4.*time_visit,age];
% X=[ones(numScans,1),time_visit,group,group.*time_visit,e4,age];
% X=[ones(numScans,1),time_visit,group,group.*time_visit,gender,age];
% X=[ones(numScans,1),time_visit,group,group.*time_visit,gender,age,e4];
% X=[ones(numScans,1),time_visit,group,group.*time_visit,education,age];
% X=[ones(numScans,1),time_visit,group,group.*time_visit,education,age,gender];
% X=[ones(numScans,1),time_visit,group,group.*time_visit,education,age,gender,e4];
clear age e4 gender education time_visit numScans;
message_FixedEffects='time_visit,group,group.*time_visit,age';
end