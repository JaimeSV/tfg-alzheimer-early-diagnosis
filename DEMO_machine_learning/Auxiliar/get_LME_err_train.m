function [err2_mean,diagn_two_GT,total_vol_stats] = get_LME_err_train(X,Y,ni_two)
total_vol_stats = lme_fit_FS(X,[1 2],Y,ni_two);

X_gr2=X;
group_two=X(:,3);

X_gr2(:,3)=1;
X_gr2(:,4)=X(:,2);
Y_gr2= ((total_vol_stats.Bhat)'*(X_gr2)')';

err=(Y-Y_gr2);


%% Subjects
numPatients=length(ni_two);
diagn_two_GT=zeros(numPatients,1);
err2_mean=zeros(numPatients,1);

begin_index=0;
for i=1:numPatients
    numImgs=ni_two(i);
    begin_index=begin_index+1;
    end_index=begin_index+numImgs-1;
    err2_mean(i,:)=mean(err(begin_index:end_index,:));
    diagn_two_GT(i)=group_two(begin_index);
    begin_index=end_index;

end

end
